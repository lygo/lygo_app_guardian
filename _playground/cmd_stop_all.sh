##!/bin/sh

# STOP WEB SERVER (guardian was launched with stop enabled)
echo '' > ./repo_updater/_guardian/stop

# STOP LAUNCHERS
echo '' > ./server_01/bin/mac/stop