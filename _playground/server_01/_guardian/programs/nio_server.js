var nio = require("nio");
var crypto = require("crypto-utils");

var VERSION = "1.1.1";


function onMessage(eventName, params) {
    console.log(VERSION, eventName, "params count:" + params.length);
    if (eventName === "file_upload" && params.length === 2) {
        var file = params[0];
        var text = crypto.decodeBase64ToText(params[1]);
        console.log(VERSION, "file: ", file, "\tdata: ", !!text ? text.length : 0);
    }
}

console.log("STARTING...", VERSION);
var server = nio.newServer(10001);
try {
    server.open();
    server.listen("*", onMessage);

    // mark server is ready
    _global.Set("server", true);

    // lock and wait until close
    server.join();
} catch (err) {
    server.close();
    console.error(VERSION, err);
}
