##!/bin/sh

# static web server (used to serve files for updates, -s=stop enable stop-by-file)
./repo_updater/guardian run -dir_work=./repo_updater/_guardian -m=production -s=stop &

# applications
./server_01/bin/mac/launcher -dir_work=./server_01/bin/mac/launcher_settings -m=production &