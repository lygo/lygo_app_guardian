# Guardian Playground #
This is a sample "playground" to simulate and better understand Guardian features and potential.

Features we are testing in this playground are:
* Auto-update of Guardian binaries
* Auto-update of Guardian programs
* Multi-Node architecture with TCP communication 

## Launcher ##

To enable binaries "live update" features you need launch guardian runtime from another application.

This sort of application is called "LAUNCHER". The launcher is an application that check for updates and launch the runtime.

### Launcher Arguments ###

* dir_start: Path of batch file that launched the LAUNCHER (absolute or relative).
* dir_app: LAUNCHER binary path (absolute or relative).
* dir_work: LAUNCHER Workspace (absolute or relative). Where launcher creates logs and find configuration files.
* m: Mode (debug, production)
* s: Stop command 

sample: 
```
./server_01/bin/mac/launcher -dir_work=./server_01/bin/mac/launcher_settings -m=production &
```

For further details about launcher [look at here](https://bitbucket.org/lygo/lygo_app_launcher/src/master/)

### Guardian Arguments ###

* dir_start: Path of batch file or binary that launched GUARDIAN (absolute or relative).
* dir_app: GUARDIAN binary path (absolute or relative).
* dir_work: GUARDIAN Workspace (absolute or relative). Where guardian creates logs, find configuration files, programs and modules.
* m: Mode (debug, production)
* s: Stop command 

sample: 
```
./myapp/guardian run -dir_work=./myapp/_guardian -m=production -s=stop &
```

## Directories ##

### Client Nodes ###

Even if a node can be both a client and a server, in this playground we assume to have a client/server architecture
with multiple clients that send/receive data from a server node (or master node).

Client Nodes are stored in folders named with a "client_" prefix.

For example all of this are client names: [`client_01, client_02, client_03, ...`]

### Server Node ###

The Server Node is named with prefix "`server_`".
Even if in this playground we have just a single server node, in 
production environment we should have many nodes and also some load balancing features.

### Update Repository ###

To test auto-update features we are using a static web server implemented on Guardian.
Even if Auto-update works also on file system, we are using a static web server to better fit at real world 
application context, where usually updates happen over internet and not in local networks.

The directory containing web server and files is "`repo_updater`".

## Directories List ##

* [repo_updater](./repo_updater): Contain static web server and resources for auto update.
* [server_01](./server_01): Guardian network server
* [client_01](./client_01): Guardian network client
* [client_02](./client_02): Guardian network client

## How to Play with Guardian ##

# QA #

```
Q. "Why did not you used Docker for this playground?"
A. We preferred create a plain and clear workspace to easily interact to.
```

```
Q. "Is it possible use Guardian as a simple automator tool?"
A. Yes, Guardian is a simple automator tool with super-powers.
```
