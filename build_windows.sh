##!/bin/sh

BASE="1.1"
# Get latest build
BUILD=$(<build_version.txt)
chrlen=${#BUILD}
if [ $chrlen = 0 ]
then
  BUILD=0
fi

echo "START BUILDING WINDOWS VERSION $BASE.$BUILD..."

## windows
env GOOS=windows GOARCH=386 go build -ldflags="-H windowsgui" -o ./_build/windows/guardian.exe main.go

# updater internal guardian
#rm ./_playground/repo_updater/guardian.exe
#cp ./_build/windows/guardian.exe ./_playground/repo_updater/
#echo "COPY VERSION $BASE.$BUILD TO ./_playground/repo_updater/"

# downloads
## RUNTIME
#rm ./_playground/repo_updater/_guardian/www/downloads/runtime/win/guardian.exe
#cp ./_build/windows/guardian.exe ./_playground/repo_updater/_guardian/www/downloads/runtime/win/
# echo "COPY VERSION $BASE.$BUILD TO ./_playground/repo_updater/_guardian/www/downloads/runtime/win/"