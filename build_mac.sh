##!/bin/sh

BASE="1.1"
# Get latest build
BUILD=$(<build_version.txt)
chrlen=${#BUILD}
if [ $chrlen = 0 ]
then
  BUILD=0
else
  BUILD=$(($BUILD + 1))
fi
echo $BUILD > ./build_version.txt
echo "START BUILDING MAC OSX VERSION $BASE.$BUILD..."

## mac
go build -o ./_build/mac/guardian main.go
echo "END BUILDING VERSION $BASE.$BUILD."

# updater internal guardian
#rm ./_playground/repo_updater/guardian
#cp ./_build/mac/guardian ./_playground/repo_updater/
#echo "COPY VERSION $BASE.$BUILD TO ./_playground/repo_updater/"

# updater internal guardian
#rm ./_playground/server_01/bin/mac/guardian
#cp ./_build/mac/guardian ./_playground/server_01/bin/mac/guardian
cp ./build_version.txt ./_build
echo "COPY VERSION $BASE.$BUILD TO ./_playground/server_01/bin/mac/guardian"

# downloads
## RUNTIME
rm ./_playground/repo_updater/_guardian/www/downloads/runtime/mac/guardian
cp ./_build/mac/guardian ./_playground/repo_updater/_guardian/www/downloads/runtime/mac/
echo $BASE.$BUILD > ./_playground/repo_updater/_guardian/www/downloads/runtime/version.txt
echo "COPY VERSION $BASE.$BUILD TO ./_playground/repo_updater/_guardian/www/downloads/runtime/mac/"