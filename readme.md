# Guardian #

![icon](./icon.png)

Guardian is a "task automator" robot-software, is a programmable, observable and distributed job orchestration 
system which allows for the scheduling, management and unattended background execution of user created tasks on Linux, 
Windows and Mac.

Job schedulers by definition are supposed to eliminate toil, a kind of work tied to running a service which is manual, 
repetitive and most importantly, automatable. 

## Author Notes ##

When I first thought at Guardian, I was thinking about a tool to maintain 
my application health (memory usage, monitor files growth, etc...). 
I was thinking about something that could preserve and defend my applications from crash
or system malfunctions.

But Guardian is first of all a robot software, and you can perform any cyclic/scheduled action you need.

## Guardian Engine Overview ##

Guardian Engine is based upon [LyGoScripting](https://bitbucket.org/lygo/lygo_ext_scripting), 
a Javascript engine with "modules" implementation.

Read this [DOCUMENTATION](./_doc/index.md) to better learn how to create programs (agents) with Guardian.
Continue below for a generic overview.

## Sample ##

```javascript
var path = require("path");

(function(){
	
    var response = {
        delimiter: path.delimiter,
        sep: path.sep,
        dirname: path.dirname("/dir/dir2/dir3/file"),
        basename: path.basename("/dir/dir2/dir3/file.html", ".html"),
        extname: path.extname("/dir/dir2/dir3/file.html"),
        format: path.format({
            "dir":"/home/dir1",
            "base":"file.html"
        }),
        isAbsolute: path.isAbsolute("/dir/dir2/dir3/file.html"),
        isAbsolute2: path.isAbsolute("file.html"),
        join: path.join("/dir/dir2", "dir3", "file.html", ".."),
        normalize: path.normalize('/foo/bar//baz/asdf/quux/..'),
        parse: path.parse('/foo/bar/baz/asdf/quux/file.txt'),
        relative: path.relative('/data/orandea/test/aaa', '/data/orandea/impl/bbb'),
        resolve: path.resolve('/foo/bar', './baz'),
        resolve2: path.resolve('/foo/bar', '/tmp/file/'),
        resolve3: path.resolve('wwwroot', 'static_files/png/', '../gif/image.gif'),

    };

    var s = JSON.stringify(response);
    console.debug(s);
    return s;

})();
```

Javascript engine support **"modules"**.

Modules can be:
* Native: written in Go. ex: `var path = require("path");`
* Scripted: written in Javascript. ex: `var mymodule = require("mymodule.js");`

Scripted modules must be stored in "./modules" directory beneath Guardian Workspace directory.

To learn more about implemented modules, please refer to 
[LyGoScripting](https://bitbucket.org/lygo/lygo_ext_scripting).

## Module "Workspace" ##

Most of the modules are declared in [LyGoScripting](https://bitbucket.org/lygo/lygo_ext_scripting), but can also be added 
at engine as custom modules.

For example, the `workspace` module is a custom module added by Guardian runtime at core engine.
Developers can use modules via `require` command as in below example: 

```javascript
var workspace = require("workspace");

// get absolute path relative at guardian workspace
// 'file' variable value: '/Users/angelogeminiani/.../_guardian/'
var file = workspace.resolve("data.csv");

// join some paths under workspace 
// 'wpSubDir' variable value: '/Users/angelogeminiani/.../_guardian/app/dir'
var wpSubDir = workspace.resolve('./', '/app', '/dir'); 

// '/Users/angelogeminiani/.../_guardian'
var dirWorkspace = workspace.resolve('.'); // workspace solved with module

// '/Users/angelogeminiani/.../_guardian'
var varWorkspace = __dir_work; // workspace solved by global variable

```

As already discussed, the module `workspace` is a custom module added 
from Guardian to [LyGoScripting](https://bitbucket.org/lygo/lygo_ext_scripting).

This module extends scripting engine adding an utility tool to access file system beneath Guardian workspace directory. 

Guardian paths are available also from global variables:
`__dir_start`, `__dir_app`, `__dir_work`, `__dir_modules`, `__dirname`, `__filename` 
(see below for further details).

## Internal Scripting Variables ##

`__dir_start`: Guardian starting directory

`__dir_app`: Guardian binary directory

`__dir_work`: Guardian Workspace

`__dir_modules`: Modules Directory

`__dirname`: Program Directory

`__filename`: Program File Name


```javascript
console.log("DIR_START = ", __dir_start);
console.log("DIR_APP = ", __dir_app);
console.log("DIR_WORK = ", __dir_work);
console.log("DIR_NAME = ", __dirname);
console.log("FILE_NAME = ", __filename);
```

## Updater Variables ##

Live update uses variables to better and simpler point at working directories.

`$dir_start`: Guardian starting directory

`$dir_app`: Guardian binary directory

`$dir_work`: Guardian Workspace

`$dir_modules`: Programs Directory

`$dir_programs`: Programs Directory

`$dir_program`: Program File Directory

Sample settings.json file for GUARDIAN runtime:
```json
{
  "silent":false,
  "log-level": "debug",
  "actions": [
    {
      "uid": "nio_server",
      "start_at": "",
      "timeline": "second:1",
      "program": "./programs/nio_server.js",
      "variables": {
        "HOST": "192.168.1.1"
      },
      "updater": {
        "version_file": "http://localhost/scripts/nio_server/version.txt",
        "package_files": [
          {
            "file": "http://localhost/scripts/nio_server/nio_server.js",
            "target": "$dir_program"
          }
        ],
        "scheduled_updates": [
          {
            "uid": "every_5_seconds",
            "start_at": "",
            "timeline": "second:5"
          }
        ]
      }
    }
  ]
}
```

Read this [DOCUMENTATION](./_doc/system/configuration.md) for further details about Configuration Files.

Read this [DOCUMENTATION](./_doc/index.md) to better learn how to create programs (agents) with Guardian.

## How to Use in Code ##

To use as a library just call:

`go get -u bitbucket.org/lygo/lygo_app_guardian`

## Dependencies ##

`go get -u bitbucket.org/lygo/lygo_commons`

`go get -u bitbucket.org/lygo/lygo_scheduler`

`go get -u bitbucket.org/lygo/lygo_ext_scripting`

`go get -u github.com/cbroglie/mustache`

### Versioning ###

Sources are versioned using git tags:

```
git tag v0.1.0
git push origin v0.1.0
```

