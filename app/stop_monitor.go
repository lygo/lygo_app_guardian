package app

import (
	"bitbucket.org/lygo/lygo_app_guardian/app/commons"
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_events"
	"sync"
	"time"
)

// ---------------------------------------------------------------------------------------------------------------------
//		t y p e
// ---------------------------------------------------------------------------------------------------------------------

type StopMonitor struct {
	root       string // where stop file is stored
	stopCmd    string
	logger     *commons.Logger
	events     *lygo_events.Emitter
	fileMux    sync.Mutex
	stopTicker *lygo_events.EventTicker
}

// ---------------------------------------------------------------------------------------------------------------------
//		c o n s t r u c t o r
// ---------------------------------------------------------------------------------------------------------------------

func NewStopMonitor(root, stopCmd string, logger *commons.Logger, events *lygo_events.Emitter) *StopMonitor {
	instance := new(StopMonitor)
	instance.root = root
	instance.stopCmd = stopCmd
	instance.logger = logger
	instance.events = events

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//		p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *StopMonitor) Start() {
	if nil != instance && len(instance.stopCmd) > 0 && nil == instance.stopTicker {
		instance.stopTicker = lygo_events.NewEventTicker(1*time.Second, func(t *lygo_events.EventTicker) {
			instance.checkStop()
			// instance.logger.Debug("Checking for stop command....")
		})
	}
}

func (instance *StopMonitor) Stop() {
	if nil != instance && nil != instance.stopTicker {
		instance.stopTicker.Stop()
		instance.stopTicker = nil
	}
}

// ---------------------------------------------------------------------------------------------------------------------
//		p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *StopMonitor) checkStop() {
	if nil != instance && len(instance.stopCmd) > 0 {
		instance.fileMux.Lock()
		defer instance.fileMux.Unlock()

		cmd := instance.stopCmd

		// check if file exists
		cmdFile := lygo_paths.Concat(instance.root, cmd)
		if b, _ := lygo_paths.Exists(cmdFile); b {
			_ = lygo_io.Remove(cmdFile)
			instance.events.EmitAsync(commons.EventOnDoStop)
		}
	}
}
