package scripting

import (
	"bitbucket.org/lygo/lygo_app_guardian/app/commons"
	"bitbucket.org/lygo/lygo_app_guardian/app/scripting/modules/workspace"
	"bitbucket.org/lygo/lygo_app_guardian/app/scripting/wrappers"
	"bitbucket.org/lygo/lygo_commons/lygo_date"
	"bitbucket.org/lygo/lygo_commons/lygo_errors"
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_ext_auth0"
	"bitbucket.org/lygo/lygo_ext_dbal/dbal_drivers"
	"bitbucket.org/lygo/lygo_ext_scripting"
	scriptCommons "bitbucket.org/lygo/lygo_ext_scripting/commons"
	"bitbucket.org/lygo/lygo_ext_scripting/modules/auth0"
	"bitbucket.org/lygo/lygo_ext_scripting/modules/dbal"
	"fmt"
	"github.com/dop251/goja"
	"github.com/gofiber/fiber/v2"
	"time"
)

//----------------------------------------------------------------------------------------------------------------------
//	c o n s t
//----------------------------------------------------------------------------------------------------------------------

const VARIABLE_PREFIX = "_"

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

type Program struct {
	name     string
	filename string
	logger   *commons.Logger
	modules  *lygo_ext_scripting.ModuleRegistry
	runtime  *lygo_ext_scripting.ScriptEngine
	script   string
	context  map[string]interface{}
}

//----------------------------------------------------------------------------------------------------------------------
//	c o n s t r u c t o r
//----------------------------------------------------------------------------------------------------------------------

func NewProgram(workspace, uid string, filename string, silent bool, variables map[string]interface{}, modules *lygo_ext_scripting.ModuleRegistry, logger *commons.Logger) (*Program, error) {
	instance := new(Program)
	if len(uid) == 0 {
		instance.name = lygo_paths.FileName(filename, false)
	} else {
		instance.name = uid
	}
	instance.modules = modules
	instance.logger = logger
	instance.filename = filename
	instance.context = make(map[string]interface{})
	if nil != variables {
		for k, v := range variables {
			instance.context[k] = v
		}
	}

	// init runtime
	if b, err := lygo_paths.Exists(filename); b {
		text, err := lygo_io.ReadTextFromFile(filename)
		if nil == err {
			instance.script = text
			instance.runtime = lygo_ext_scripting.NewEngine()
			instance.runtime.Root = workspace // program workspace
			instance.runtime.Name = instance.name
			instance.runtime.Silent = silent // do not log at console
			instance.runtime.LogLevel = logger.GetLevel()

			// enable runtime to use modules
			runtimeContext := modules.Start(instance.runtime)
			extendEngine(instance.runtime, runtimeContext)

			// remove log if any
			_ = lygo_io.Remove(lygo_paths.Concat(workspace, instance.name+".log"))
		} else {
			return nil, lygo_errors.Prefix(err, "Error reading program file:")
		}
	} else {
		return nil, lygo_errors.Prefix(err, "Invalid program file error:")
	}

	return instance, nil
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *Program) Context() *Context {
	return GetContext(instance.name)
}

func (instance *Program) GlobalContext() *Context {
	return GetGlobalContext()
}

func (instance *Program) Run(functionName string, args ...interface{}) (interface{}, bool, error) {
	if nil != instance && nil != instance.runtime && len(instance.script) > 0 {

		instance.logger.Debug(fmt.Sprintf("[%v] RUN '%v'", lygo_date.FormatDate(time.Now(), "yyyyMMdd HH:mm:ss"), instance.name))

		runtime := instance.runtime
		script := instance.script

		// init context
		runtime.Set(VARIABLE_PREFIX+"context", instance.Context())
		runtime.Set(VARIABLE_PREFIX+"global", instance.GlobalContext()) // shared between programs

		// get function args, if any
		params := instance.extractValues(args...)

		// add variables declared in context (may come from action configuration "variables")
		updateRuntimeContext(instance.runtime, instance.context)

		// EXECUTE...
		value, functionFound, err := instance.run(script, functionName, params...)

		// convert and return value
		var v interface{}
		if nil != value {
			v = value.Export()
		}
		return v, functionFound, err
	}
	return nil, false, nil
}

func (instance *Program) Close() {
	if nil != instance && nil != instance.runtime {
		instance.runtime.Close()
	}
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *Program) run(script string, functionName string, params ...goja.Value) (goja.Value, bool, error) {
	runtime := instance.runtime
	functionFound := false
	value, err := runtime.RunScript(script)
	if nil == err && nil != value && len(functionName) > 0 {
		function := runtime.GetMemberAsCallable(value, functionName)
		if nil != function {
			// override value and error
			functionFound = true
			value, err = function(nil, params...)
		}
	}
	return value, functionFound, err
}

func (instance *Program) extractValues(args ...interface{}) []goja.Value {
	response := make([]goja.Value, 0)
	if len(args) > 0 {
		runtime := instance.runtime
		for _, arg := range args {
			if ctx, b := arg.(*fiber.Ctx); b {
				req := wrappers.WrapRequest(runtime, ctx)
				res := wrappers.WrapResponse(runtime, ctx)

				// add to context too
				instance.context["req"] = req
				instance.context["res"] = res

				response = append(response, req, res)
			} else {
				response = append(response, runtime.ToValue(arg))
			}
		}
	}
	return response
}

// extendEngine register custom modules and add custom tools
func extendEngine(_ *lygo_ext_scripting.ScriptEngine,
	context *scriptCommons.RuntimeContext) {

	// workspace path helper
	workspace.Enable(context)

}

func wrapContext(runtime *lygo_ext_scripting.ScriptEngine, context map[string]interface{}) {
	if len(context) > 0 {
		for k, v := range context {
			if e, b := v.(error); b {
				context[k] = e.Error()
			} else if db, b := v.(dbal_drivers.IDatabase); b {
				wrap := dbal.WrapDbal(runtime.Runtime(), db)
				context[k] = wrap
			} else if a0, b := v.(*lygo_ext_auth0.Auth0); b {
				wrap := auth0.WrapAuth0(runtime.Runtime(), a0).Value()
				context[k] = wrap
			} else if m, b := v.(map[string]interface{}); b {
				wrapContext(runtime, m)
			}
		}
	}
}

func updateRuntimeContext(runtime *lygo_ext_scripting.ScriptEngine, context map[string]interface{}) {
	if len(context) > 0 {
		wrapContext(runtime, context)
		for k, v := range context {
			if nil != v {
				runtime.Set(VARIABLE_PREFIX+k, v)
				_ = runtime.SetRootContextValue(k, v)
			}
		}
		/**
		for k, v := range context {
			if nil != v {
				// transform
				if e, b := v.(error); b {
					runtime.Set(VARIABLE_PREFIX+k, e.Error())
					_ = runtime.SetRootContextValue(k, e.Error())
				} else if db, b := v.(drivers.IDatabase); b {
					wrap := dbal.WrapDbal(runtime.Runtime(), db)
					runtime.Set(VARIABLE_PREFIX+k, wrap)
					_ = runtime.SetRootContextValue(k, wrap)
				} else if a0, b := v.(*lygo_ext_auth0.Auth0); b {
					wrap := auth0.WrapAuth0(runtime.Runtime(), a0).Value()
					runtime.Set(VARIABLE_PREFIX+k, wrap)
					_ = runtime.SetRootContextValue(k, wrap)
				} else {
					runtime.Set(VARIABLE_PREFIX+k, v)
					_ = runtime.SetRootContextValue(k, v)
				}
			}
		}**/
	}
}
