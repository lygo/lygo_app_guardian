package scripting

import "sync"

//----------------------------------------------------------------------------------------------------------------------
//	ContextManager
//----------------------------------------------------------------------------------------------------------------------

var singleton *ContextManager

type ContextManager struct {
	data map[string]*Context
	mux  sync.Mutex
}

func NewContextManager() *ContextManager {
	instance := new(ContextManager)
	instance.data = make(map[string]*Context)

	return instance
}

func (instance *ContextManager) get(key string) *Context {
	instance.mux.Lock()
	defer instance.mux.Unlock()
	if _, b := instance.data[key]; !b {
		instance.data[key] = new(Context)
		instance.data[key].data = make(map[string]interface{})
	}
	return instance.data[key]
}

//----------------------------------------------------------------------------------------------------------------------
//	Context
//----------------------------------------------------------------------------------------------------------------------

type Context struct {
	data map[string]interface{}
	mux  sync.RWMutex
}

func (instance *Context) Get(key string) interface{} {
	instance.mux.RLock()
	defer instance.mux.RUnlock()
	if v, b := instance.data[key]; b {
		return v
	}
	return false
}

func (instance *Context) Set(key string, value interface{}) {
	instance.mux.Lock()
	defer instance.mux.Unlock()

	instance.data[key] = value
}

func (instance *Context) Delete(key string) interface{} {
	instance.mux.Lock()
	defer instance.mux.Unlock()

	if v, b := instance.data[key]; b {
		delete(instance.data, key)
		return v
	}
	return false
}

func (instance *Context) Keys() []string {
	instance.mux.RLock()
	defer instance.mux.RUnlock()
	response := make([]string, 0)
	for k, _ := range instance.data {
		response = append(response, k)
	}
	return response
}

//----------------------------------------------------------------------------------------------------------------------
//	S T A T I C
//----------------------------------------------------------------------------------------------------------------------

func GetContext(key string) *Context {
	if nil == singleton {
		singleton = NewContextManager()
	}
	return singleton.get(key)
}

func GetGlobalContext() *Context {
	if nil == singleton {
		singleton = new(ContextManager)
		singleton.data = make(map[string]*Context)
	}
	return singleton.get("global")
}
