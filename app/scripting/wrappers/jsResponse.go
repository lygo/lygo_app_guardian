package wrappers

import (
	"bitbucket.org/lygo/lygo_commons/lygo_crypto"
	"bitbucket.org/lygo/lygo_ext_scripting"
	"bitbucket.org/lygo/lygo_ext_scripting/commons"
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/dop251/goja"
	"github.com/gofiber/fiber/v2"
)

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

type JsHttpResponse struct {
	runtime       *lygo_ext_scripting.ScriptEngine
	object        *goja.Object
	data          bytes.Buffer
	ctxHttp       *fiber.Ctx
	statusCode    int
	statusMessage string
	header        map[string]string
}

//----------------------------------------------------------------------------------------------------------------------
//	JsHttpResponse
//----------------------------------------------------------------------------------------------------------------------

func WrapResponse(runtime *lygo_ext_scripting.ScriptEngine, ctx interface{}) goja.Value {
	instance := new(JsHttpResponse)
	instance.runtime = runtime
	if v, b := ctx.(*fiber.Ctx); b {
		instance.ctxHttp = v
	}

	instance.object = instance.runtime.NewObject()
	instance.export()

	return instance.value()
}

//----------------------------------------------------------------------------------------------------------------------
//	e x p o s e d
//----------------------------------------------------------------------------------------------------------------------

func (instance *JsHttpResponse) length(_ goja.FunctionCall) goja.Value {
	if instance.data.Len() > 0 {
		return instance.runtime.ToValue(instance.data.Len())
	}
	return instance.runtime.ToValue(0)
}

func (instance *JsHttpResponse) bytes(_ goja.FunctionCall) goja.Value {
	if instance.data.Len() > 0 {
		return instance.runtime.ToValue(instance.data.Bytes())
	}
	return instance.runtime.ToValue([]byte{})
}

func (instance *JsHttpResponse) send(call goja.FunctionCall) goja.Value {
	if nil != instance.ctxHttp {
		var data interface{}
		var contentType string
		switch len(call.Arguments) {
		case 1:
			data = commons.GetExport(call, 0)
			contentType = fiber.MIMETextPlain
		case 2:
			data = commons.GetExport(call, 0)
			contentType = commons.GetString(call, 1)
		default:
			data = ""
			contentType = fiber.MIMETextPlain
		}

		if nil != data {
			// HTTP
			if nil != instance.ctxHttp {
				err := writeHttp(instance.ctxHttp, contentType, data)
				if nil != err {
					panic(instance.runtime.NewTypeError(err.Error()))
				}
			}

		} else {
			panic(instance.runtime.NewTypeError(commons.ErrorMissingParam))
		}
	}
	return goja.Undefined()
}

func (instance *JsHttpResponse) text(call goja.FunctionCall) goja.Value {
	if nil != instance.ctxHttp {
		var data interface{}
		var contentType string
		switch len(call.Arguments) {
		case 1:
			data = commons.GetExport(call, 0)
			contentType = fiber.MIMETextPlain
		default:
			data = ""
			contentType = fiber.MIMETextPlain
		}

		if nil != data {
			// HTTP
			if nil != instance.ctxHttp {
				err := writeHttp(instance.ctxHttp, contentType, data)
				if nil != err {
					panic(instance.runtime.NewTypeError(err.Error()))
				}
			}
		} else {
			panic(instance.runtime.NewTypeError(commons.ErrorMissingParam))
		}
	}
	return instance.runtime.ToValue("")
}

func (instance *JsHttpResponse) html(call goja.FunctionCall) goja.Value {
	if nil != instance.ctxHttp {
		var data interface{}
		var contentType string
		switch len(call.Arguments) {
		case 1:
			data = commons.GetExport(call, 0)
			contentType = fiber.MIMETextHTML
		default:
			data = ""
			contentType = fiber.MIMETextHTML
		}

		if nil != data {
			// HTTP
			if nil != instance.ctxHttp {
				err := writeHttp(instance.ctxHttp, contentType, data)
				if nil != err {
					panic(instance.runtime.NewTypeError(err.Error()))
				}
			}
		} else {
			panic(instance.runtime.NewTypeError(commons.ErrorMissingParam))
		}
	}
	return instance.runtime.ToValue("")
}

func (instance *JsHttpResponse) json(call goja.FunctionCall) goja.Value {
	if nil != instance {
		value := commons.GetExport(call, 0)
		if nil != value {
			if nil != instance.ctxHttp {
				err := instance.ctxHttp.JSON(value)
				if nil != err {
					panic(instance.runtime.NewTypeError(err.Error()))
				}
			}

		}
	}
	return goja.Undefined()
}

func (instance *JsHttpResponse) image(call goja.FunctionCall) goja.Value {
	if nil != instance.ctxHttp {
		var data interface{}
		var contentType string
		switch len(call.Arguments) {
		case 1:
			data = commons.GetExport(call, 0)
			contentType = "image/png"
		default:
			data = ""
			contentType = "image/png"
		}

		if nil != data {
			// try convert data
			if s, b := data.(string); b {
				bdata, err := lygo_crypto.DecodeBase64(s)
				if nil == err {
					data = bdata
				}
			}
			// HTTP
			if nil != instance.ctxHttp {
				err := writeHttp(instance.ctxHttp, contentType, data)
				if nil != err {
					panic(instance.runtime.NewTypeError(err.Error()))
				}
			}
		} else {
			panic(instance.runtime.NewTypeError(commons.ErrorMissingParam))
		}
	}
	return instance.runtime.ToValue("")
}

func (instance *JsHttpResponse) status(call goja.FunctionCall) goja.Value {
	if nil != instance {
		value := commons.GetInt(call, 0)
		if value > 0 {
			if nil != instance.ctxHttp {
				err := instance.ctxHttp.SendStatus(int(value))
				if nil != err {
					panic(instance.runtime.NewTypeError(err.Error()))
				}
			}
		}
	}
	return goja.Undefined()
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *JsHttpResponse) value() goja.Value {
	if nil != instance.object {
		return instance.object
	}
	return goja.Undefined()
}

func (instance *JsHttpResponse) init() {
	o := instance.object
	if nil != o {
		_ = o.Set("size", instance.data.Len())
		_ = o.Set("header", instance.runtime.ToValue(instance.header))
		//_ = o.Set("body", instance.runtime.ToValue(instance.data.String()))
		if instance.statusCode > 0 {
			_ = o.Set("statusCode", instance.statusCode)
			_ = o.Set("statusMessage", instance.statusMessage)
			_ = o.Set("status", fmt.Sprintf("%v:%v", instance.statusCode, instance.statusMessage))
		}
	}
}

func (instance *JsHttpResponse) export() {
	o := instance.object
	_ = o.Set("send", instance.send)
	_ = o.Set("text", instance.text)
	_ = o.Set("html", instance.html)
	_ = o.Set("json", instance.json)
	_ = o.Set("status", instance.status)
}

//----------------------------------------------------------------------------------------------------------------------
//	S T A T I C
//----------------------------------------------------------------------------------------------------------------------

func writeHttp(ctx *fiber.Ctx, contentType string, data interface{}) error {
	if nil != ctx {
		var err error
		ctx.Response().Header.SetContentType(contentType)
		if v, b := data.(string); b {
			_, err = ctx.Response().BodyWriter().Write([]byte(v))
		} else if v, b := data.([]byte); b {
			_, err = ctx.Response().BodyWriter().Write(v)
		} else if v, b := data.([]uint8); b {
			_, err = ctx.Response().BodyWriter().Write(v)
		} else if v, b := data.(map[string]interface{}); b {
			raw, err := json.Marshal(v)
			if err == nil {
				_, err = ctx.Response().BodyWriter().Write(raw)
			}
		}
		return err
	}
	return nil
}
