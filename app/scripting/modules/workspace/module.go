package workspace

import (
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_ext_scripting/commons"
	"bitbucket.org/lygo/lygo_ext_scripting/modules/defaults/require"
	"github.com/dop251/goja"
	"path/filepath"
)

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

const NAME = "workspace"

type ModuleWorkspace struct {
	runtime *goja.Runtime
}

//----------------------------------------------------------------------------------------------------------------------
//	e x p o s e d
//----------------------------------------------------------------------------------------------------------------------

// workspace.resolve([...paths])
// The path.resolve() method resolves a sequence of paths or path segments into an absolute
// workspace path.
// The given sequence of paths is processed from right to left, with each subsequent path prepended
// until an absolute path is constructed. For instance, given the sequence of path
// segments: /foo, /bar, baz, calling path.resolve('/foo', '/bar', 'baz') would
// return /bar/baz because 'baz' is not an absolute path but '/bar' + '/' + 'baz' is.
// If after processing all given path segments an absolute path has not yet been generated,
// the current working directory is used.
// The resulting path is normalized and trailing slashes are removed unless the path is resolved
// to the root directory.
// Zero-length path segments are ignored.
// If no path segments are passed, path.resolve() will return the absolute path of the current
// working directory.
func (instance *ModuleWorkspace) resolve(call goja.FunctionCall) goja.Value {
	val := lygo_paths.WorkspacePath(".")
	if len(call.Arguments) > 0 {
		paths := make([]string, 0)
		for i, v := range call.Arguments {
			path := v.String()
			if i == 0 && !filepath.IsAbs(path) {
				path = lygo_paths.WorkspacePath(path)
			}
			paths = append(paths, path)
		}
		if len(paths) > 0 {
			val = filepath.Join(paths...)
		}
	}
	return instance.runtime.ToValue(val)
}

//----------------------------------------------------------------------------------------------------------------------
//	S T A T I C
//----------------------------------------------------------------------------------------------------------------------

func load(runtime *goja.Runtime, module *goja.Object, _ ...interface{}) {
	instance := &ModuleWorkspace{
		runtime: runtime,
	}

	o := module.Get("exports").(*goja.Object)

	// path
	_ = o.Set("resolve", instance.resolve)

}

func Enable(ctx *commons.RuntimeContext) {
	// register
	require.RegisterNativeModule(NAME, &commons.ModuleInfo{
		Context: ctx,
		Loader:  load,
	})
}
