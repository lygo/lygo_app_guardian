package tasks

import (
	"bitbucket.org/lygo/lygo_app_guardian/app/commons"
	"bitbucket.org/lygo/lygo_app_guardian/app/secure"
	"bitbucket.org/lygo/lygo_app_guardian/app/variables"
	"bitbucket.org/lygo/lygo_commons/lygo_errors"
	"bitbucket.org/lygo/lygo_commons/lygo_fmt"
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_ext_scripting"
	"fmt"
	"os"
	"sync"
	"time"
)

// ---------------------------------------------------------------------------------------------------------------------
//		t y p e
// ---------------------------------------------------------------------------------------------------------------------

type TaskManager struct {
	dirWork         string
	dirTasks        string // where monitor objects are stored
	logger          *commons.Logger
	settings        *commons.GuardianSettings
	secureManager   *secure.AppSecure
	variableManager *variables.AppVariables
	modules         *lygo_ext_scripting.ModuleRegistry

	programs *TaskList
	fileMux  sync.Mutex
	infoPid  int
	infoPPid int
	taskPool *TaskPool
}

// ---------------------------------------------------------------------------------------------------------------------
//		c o n s t r u c t o r
// ---------------------------------------------------------------------------------------------------------------------

func NewTaskManager(dirWork string, logger *commons.Logger, secureManager *secure.AppSecure,
	variables *variables.AppVariables,
	settings *commons.GuardianSettings, modules *lygo_ext_scripting.ModuleRegistry) *TaskManager {

	// create instance
	instance := new(TaskManager)
	instance.dirWork = dirWork
	instance.dirTasks = lygo_paths.Concat(instance.dirWork, "tasks")
	instance.logger = logger
	instance.secureManager = secureManager
	instance.settings = settings
	instance.modules = modules
	instance.variableManager = variables
	instance.programs = NewTaskList()
	instance.infoPid = os.Getpid()   // process pid
	instance.infoPPid = os.Getppid() // parent process pid
	instance.taskPool = NewTaskPool()

	_ = lygo_paths.Mkdir(instance.dirTasks + "/")

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//		p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *TaskManager) Stop() {
	if nil != instance && nil != instance.programs {
		instance.programs.Stop()
	}
}

func (instance *TaskManager) StopTask(uid string) {
	if nil != instance && nil != instance.programs {
		instance.programs.StopTask(uid)
	}
}

func (instance *TaskManager) Count(uid string) int {
	if nil != instance && nil != instance.programs {
		return instance.programs.Count(uid)
	}
	return 0
}

func (instance *TaskManager) Variables() *variables.AppVariables {
	if nil != instance {
		return instance.variableManager
	}
	return nil
}

func (instance *TaskManager) RunSingle(uid string, schedule map[string]interface{}, action *commons.Action) {
	if len(action.Program) > 0 {
		/**/
		resp := instance.run(uid, schedule, action, true, "")
		instance.writeInfoAsync(uid, schedule, true, resp.Err)
		if nil != resp.Err {
			instance.logger.Error(lygo_errors.Prefix(resp.Err, fmt.Sprintf("Program Error for '%v'", action.Program)))
		}
	} else {
		// fmt.Println("Running: " , schedule.Uid, programFile)
		// update task info into tasks folder
		instance.writeInfoAsync(uid, schedule, false, nil)
	}
}

func (instance *TaskManager) Run(uid string, action *commons.Action,
	functionName string, args ...interface{}) *GuardianTaskResponse {
	if len(action.Program) > 0 {
		/**/
		resp := instance.run(uid, nil, action, false, functionName, args...)
		if nil != resp.Err {
			instance.logger.Error(lygo_errors.Prefix(resp.Err, fmt.Sprintf("Program Error for '%v'", action.Program)))
		}
		return resp
	}
	return nil
}

// ---------------------------------------------------------------------------------------------------------------------
//		p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *TaskManager) run(uid string, schedule map[string]interface{}, action *commons.Action,
	singleInstance bool, functionName string, args ...interface{}) *GuardianTaskResponse {
	if nil != instance && nil!=action && action.IsEnabled(){
		ready := !(singleInstance && instance.taskPool.Count(uid) > 0)
		if ready {
			// creates task
			task := NewGuardianTask(uid, action, schedule, instance.logger, instance.secureManager, instance.modules,
				instance.variableManager)
			task.Silent = instance.settings.Silent
			task.FunctionName = functionName
			task.FunctionArgs = args

			if singleInstance {
				instance.taskPool.RunSingle(task) // LIMITED AMOUNT OF CONCURRENT
			} else {
				instance.taskPool.Run(task) // LIMITED AMOUNT OF CONCURRENT
			}

			//instance.taskChan <- task
			return task.Response()
		}
	}
	return &GuardianTaskResponse{
		Executed: false,
	}
}

func (instance *TaskManager) writeInfoAsync(uid string, schedule map[string]interface{}, executed bool, err error) {
	if nil != instance {
		go instance.writeInfo(uid, schedule, executed, err)
	}
}

func (instance *TaskManager) writeInfo(uid string, schedule map[string]interface{}, executed bool, err error) {
	if nil != instance {
		instance.fileMux.Lock()
		defer instance.fileMux.Unlock()

		name := uid + ".json"
		data := map[string]interface{}{
			"settings":     schedule,
			"timestamp":    time.Now().Unix(),
			"time":         lygo_fmt.FormatDate(time.Now(), "yyyyMMdd HH:mm:ss"),
			"executed":     executed, // if false, program was already running
			"launcher-pid": instance.infoPPid,
			"pid":          instance.infoPid,
		}

		if nil != err {
			data["error"] = err.Error()
		} else {
			data["error"] = ""
		}

		filename := lygo_paths.Concat(instance.dirTasks, name)
		_, _ = lygo_io.WriteBytesToFile(lygo_json.Bytes(data), filename)
	}
}
