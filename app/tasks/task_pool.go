package tasks

import "bitbucket.org/lygo/lygo_commons/lygo_async"

const (
	MAX = 100
)

// ---------------------------------------------------------------------------------------------------------------------
//		t y p e
// ---------------------------------------------------------------------------------------------------------------------

type TaskCallback func(resp *GuardianTaskResponse)

type TaskPool struct {
	programs *TaskList
	pool     *lygo_async.ConcurrentPool
}

func NewTaskPool() *TaskPool {
	instance := new(TaskPool)
	instance.programs = NewTaskList()
	instance.pool = lygo_async.NewConcurrentPool(MAX)

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//		p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *TaskPool) Count(uid string) int {
	if nil != instance {
		return instance.programs.Count(uid)
	}
	return 0
}

func (instance *TaskPool) RunSingle(task *GuardianTask) {
	if nil != instance {
		instance.run(true, task)
	}
}

func (instance *TaskPool) Run(task *GuardianTask) {
	if nil != instance {
		instance.run(false, task)
	}
}

// ---------------------------------------------------------------------------------------------------------------------
//		p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *TaskPool) run(singleInstance bool, task *GuardianTask) {
	if singleInstance && instance.programs.Count(task.uid) > 0 {
		// already running
		return
	}
	instance.pool.Run(func() error {
		instance.programs.Add(task) // running
		if !task.KeepAlive() {
			defer instance.programs.Remove(task) // not running
		}

		task.Run()
		return nil // error are not cached into pool
	})
}
