package tasks

import (
	"bitbucket.org/lygo/lygo_app_guardian/app/commons"
	"bitbucket.org/lygo/lygo_commons/lygo_exec/console_program"
)

type TaskRtPython struct {
	programFile string
	programArgs []interface{}
	settings    map[string]interface{}
}

//----------------------------------------------------------------------------------------------------------------------
//	c o n s t r u c t o r
//----------------------------------------------------------------------------------------------------------------------

func NewTaskRtPython(programFile string, programArgs []interface{}, settings map[string]interface{}) *TaskRtPython {
	instance := new(TaskRtPython)
	instance.programFile = programFile
	instance.programArgs = programArgs
	instance.settings = settings

	instance.init()

	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *TaskRtPython) Run() (interface{}, error) {
	if nil != instance {
		if len(instance.programFile) == 0 {
			return nil, commons.MissingFileNameError
		}

		args := getArguments(instance.programArgs)
		program := console_program.NewPythonProgram(instance.programFile)

		exec, err := program.RunAsync(args...)
		if nil != err {
			return nil, err
		}
		err = exec.Wait()
		if nil != err {
			return nil, err
		}

		return exec.StdOutJson(), nil
	}
	return nil, nil
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *TaskRtPython) init() {
	if nil != instance.settings {

	}
}

//----------------------------------------------------------------------------------------------------------------------
//	S T A T I C
//----------------------------------------------------------------------------------------------------------------------
