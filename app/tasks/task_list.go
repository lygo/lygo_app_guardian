package tasks

import "sync"

type TaskList struct {
	list map[string]map[string]*GuardianTask
	mux  sync.Mutex
}

func NewTaskList() *TaskList {
	instance := new(TaskList)
	instance.list = make(map[string]map[string]*GuardianTask)
	return instance
}

func (instance *TaskList) Stop() {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()
		for _, tasks := range instance.list {
			for _, task := range tasks {
				if nil != task && task.IsRunning() {
					task.Close()
				}
			}
		}
		instance.list = make(map[string]map[string]*GuardianTask)
	}
}

func (instance *TaskList) StopTask(uid string) {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()
		for key, tasks := range instance.list {
			if uid == key && len(tasks) > 0 {
				for _, task := range tasks {
					if task.IsRunning() {
						delete(instance.list, uid)
						task.Close()
						break
					}
				}
			}
		}
	}
}

func (instance *TaskList) Count(uid string) int {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		if v, b := instance.list[uid]; b {
			return len(v)
		}
	}
	return 0
}

func (instance *TaskList) Add(task *GuardianTask) {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		uid := task.Uid()
		uuid := task.Uuid()
		if _, b := instance.list[uid]; !b {
			instance.list[uid] = make(map[string]*GuardianTask)
		}
		instance.list[uid][uuid] = task
	}
}

func (instance *TaskList) Remove(task *GuardianTask) {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		uid := task.Uid()
		uuid := task.Uuid()
		if m, b := instance.list[uid]; b {
			if task,b := m[uuid];b{
				if task.IsRunning(){
					task.Close()
				}
				delete(m, uuid)
				if len(m) == 0 {
					delete(instance.list, uid)
				}
			}
		}
	}
}
