package tasks

import (
	"bitbucket.org/lygo/lygo_app_guardian/app/http"
	"bitbucket.org/lygo/lygo_commons/lygo_conv"
	"bitbucket.org/lygo/lygo_commons/lygo_reflect"
	"fmt"
	"github.com/gofiber/fiber/v2"
)

//----------------------------------------------------------------------------------------------------------------------
//	S T A T I C
//----------------------------------------------------------------------------------------------------------------------

func getArguments(args []interface{}) []string {
	response := make([]string, 0)
	params := getContextParameters(args)
	data := lygo_reflect.GetArrayOfString(params, "data")
	if nil == data {
		for k, v := range params {
			response = append(response, fmt.Sprintf("%v=%v", k, lygo_conv.ToString(v)))
		}
	} else {
		response = append(response, data...)
	}

	return response
}

func getContextParameters(args []interface{}) map[string]interface{} {
	for _, arg := range args {
		if ctx, b := arg.(*fiber.Ctx); b {
			return http.Params(ctx, true) //params(ctx, true)
		}
	}
	return map[string]interface{}{}
}
