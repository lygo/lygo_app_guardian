package tasks

import (
	"bitbucket.org/lygo/lygo_app_guardian/app/commons"
	"bitbucket.org/lygo/lygo_app_guardian/app/database"
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_ext_dbal/dbal_drivers"
	"errors"
)

type TaskRtArango struct {
	programFile string
	programArgs []interface{}
	settings    map[string]interface{}

	database interface{}
}

//----------------------------------------------------------------------------------------------------------------------
//	c o n s t r u c t o r
//----------------------------------------------------------------------------------------------------------------------

func NewTaskRtArango(programFile string, programArgs []interface{}, settings map[string]interface{}) *TaskRtArango {
	instance := new(TaskRtArango)
	instance.programFile = programFile
	instance.programArgs = programArgs
	instance.settings = settings

	instance.init()

	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *TaskRtArango) Run() (interface{}, error) {
	if nil != instance {
		if len(instance.programFile) == 0 {
			return nil, commons.MissingFileNameError
		}

		// read file
		query, err := lygo_io.ReadTextFromFile(instance.programFile)
		if nil != err {
			return nil, err
		}

		if db, b := instance.database.(dbal_drivers.IDatabase); b {
			// retrieve parameters from http request context
			paramsAll := getContextParameters(instance.programArgs)
			names := database.QueryParamNames(query)
			params := map[string]interface{}{}
			for _, v := range names {
				params[v] = paramsAll[v]
			}
			result, queryErr := db.ExecNative(query, params)
			if nil != queryErr {
				// reset database to ensure connection is not corrupted
				dbal_drivers.Cache().Remove(db.Uid())
			}
			return result, queryErr
		} else if err, b := instance.database.(error); b {
			return nil, err
		} else if s, b := instance.database.(string); b {
			return nil, errors.New(s)
		} else {
			return nil, commons.InvalidConfigurationError
		}
	}
	return nil, nil
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *TaskRtArango) init() {
	if nil != instance.settings {
		if v, b := instance.settings["database"]; b {
			instance.database = v
		}
	}
}

