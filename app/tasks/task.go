package tasks

import (
	"bitbucket.org/lygo/lygo_app_guardian/app/commons"
	"bitbucket.org/lygo/lygo_app_guardian/app/database"
	"bitbucket.org/lygo/lygo_app_guardian/app/scripting"
	"bitbucket.org/lygo/lygo_app_guardian/app/secure"
	"bitbucket.org/lygo/lygo_app_guardian/app/variables"
	"bitbucket.org/lygo/lygo_commons/lygo_errors"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_commons/lygo_rnd"
	"bitbucket.org/lygo/lygo_ext_scripting"
	"errors"
	"fmt"
	"path/filepath"
	"time"
)

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

type GuardianTaskResponse struct {
	Executed         bool
	ExecutedFunction bool
	Value            interface{}
	Err              error
	StartAt          time.Time
	EndAt            time.Time
	Elapsed          time.Duration
}

type GuardianTask struct {
	Silent       bool
	FunctionName string
	FunctionArgs []interface{}

	uuid          string
	action        *commons.Action
	logger        *commons.Logger
	secureManager *secure.AppSecure
	modules       *lygo_ext_scripting.ModuleRegistry
	programFile   string
	programDir    string
	workspacePath string
	program       *scripting.Program
	uid           string
	schedule      map[string]interface{}
	variables     map[string]interface{}
	chanResponse  chan *GuardianTaskResponse

	database *database.DatabaseManager

	// mux      sync.Mutex
}

//----------------------------------------------------------------------------------------------------------------------
//	c o n s t r u c t o r
//----------------------------------------------------------------------------------------------------------------------

func NewGuardianTask(uid string, action *commons.Action, schedule map[string]interface{}, logger *commons.Logger, secureManager *secure.AppSecure,
	modules *lygo_ext_scripting.ModuleRegistry, variables *variables.AppVariables) *GuardianTask {
	// initialize instance
	instance := new(GuardianTask)
	instance.uuid = fmt.Sprintf("%v:%v", uid, lygo_rnd.Uuid()) // instance id
	instance.uid = uid
	instance.action = action
	instance.logger = logger
	instance.secureManager = secureManager
	instance.modules = modules
	instance.workspacePath = lygo_paths.GetWorkspacePath()
	instance.programFile = lygo_paths.WorkspacePath(action.Program)
	instance.programDir = filepath.Dir(instance.programFile)
	instance.chanResponse = make(chan *GuardianTaskResponse, 1)
	instance.variables = variables.Extend(action.Variables)
	instance.variables[commons.VarFilename] = instance.programFile
	instance.variables[commons.VarDirname] = instance.programDir
	instance.variables[commons.VarUID] = uid
	instance.schedule = map[string]interface{}{}
	if nil != schedule {
		instance.schedule = schedule
	}
	instance.database = database.NewDatabaseManager(instance.logger)

	// check passed variables for known special variables
	instance.initVariables()

	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *GuardianTask) Uuid() string {
	if nil != instance {
		return instance.uuid
	}
	return ""
}

func (instance *GuardianTask) Uid() string {
	if nil != instance {
		return instance.uid
	}
	return ""
}

func (instance *GuardianTask) IsRunning() bool {
	return nil != instance.program
}

func (instance *GuardianTask) KeepAlive() bool {
	return nil != instance.action && instance.action.KeepAlive
}

func (instance *GuardianTask) GetRuntimeName() string {
	if nil != instance {
		return instance.action.GetRuntimeName()
	}
	return ""
}

func (instance *GuardianTask) RunAsync() {
	go func() {
		instance.chanResponse <- instance.run()
	}()
}

func (instance *GuardianTask) Run() {
	instance.chanResponse <- instance.run()
}

func (instance *GuardianTask) Response() *GuardianTaskResponse {
	if nil != instance {
		response := <-instance.chanResponse
		instance.chanResponse = make(chan *GuardianTaskResponse, 1)
		return response
	}
	return &GuardianTaskResponse{}
}

func (instance *GuardianTask) Close() {
	if nil != instance {
		instance.close()
	}
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *GuardianTask) initVariables() {
	/**
	if value, b := instance.variables["database"]; b {
		if m, b := value.(map[string]interface{}); b {
			db, err := instance.database.Get(m)
			if nil != err {
				instance.variables["database"] = errors.New(err.Error())
			} else {
				instance.variables["database"] = db
			}
		}
	}**/

	instance.transformData(instance.variables)

	// secure manager
	if instance.secureManager.Enabled() {
		instance.variables["auth0"] = instance.secureManager.Auth0()
	}
}

func (instance *GuardianTask) transformData(target map[string]interface{}) {
	for k, v := range target {
		if m, b := v.(map[string]interface{}); b {
			if k == "database" {
				db, err := instance.database.Get(m)
				if nil != err {
					target[k] = errors.New(err.Error())
				} else {
					target[k] = db
				}
			} else {
				instance.transformData(m)
			}
		}
	}
}

func (instance *GuardianTask) run() *GuardianTaskResponse {
	runtimeName := instance.GetRuntimeName()
	switch runtimeName {
	case "js":
		// JAVASCRIPT
		return instance.runJavascript()
	case "aql":
		// ARANGO QUERY
		return instance.runArango()
	case "sql":
		// ARANGO QUERY
		return instance.runSQL()
	case "py":
		// PYTHON
		return instance.runPython()
	}
	return &GuardianTaskResponse{
		Executed: false,
		Err:      lygo_errors.Prefix(commons.RuntimeNotSupportedError, fmt.Sprintf("Runtime Name '%v': ", runtimeName)),
	}
}

func (instance *GuardianTask) runArango() *GuardianTaskResponse {
	if nil != instance {
		response := &GuardianTaskResponse{
			Executed: true,
			StartAt:  time.Now(),
		}

		rt := NewTaskRtArango(instance.programFile, instance.FunctionArgs, instance.variables)
		value, err := rt.Run()

		response.Value = value
		response.Err = err
		response.EndAt = time.Now()
		response.Elapsed = response.EndAt.Sub(response.StartAt)

		return response
	}

	return &GuardianTaskResponse{
		Executed: false,
	}
}

func (instance *GuardianTask) runSQL() *GuardianTaskResponse {
	if nil != instance {
		response := &GuardianTaskResponse{
			Executed: true,
			StartAt:  time.Now(),
		}

		rt := NewTaskRtSQL(instance.programFile, instance.FunctionArgs, instance.variables)
		value, err := rt.Run()

		response.Value = value
		response.Err = err
		response.EndAt = time.Now()
		response.Elapsed = response.EndAt.Sub(response.StartAt)

		return response
	}

	return &GuardianTaskResponse{
		Executed: false,
	}
}

func (instance *GuardianTask) runPython() *GuardianTaskResponse {
	if nil != instance {
		response := &GuardianTaskResponse{
			Executed: true,
			StartAt:  time.Now(),
		}

		rt := NewTaskRtPython(instance.programFile, instance.FunctionArgs, instance.variables)
		value, err := rt.Run()

		response.Value = value
		response.Err = err
		response.EndAt = time.Now()
		response.Elapsed = response.EndAt.Sub(response.StartAt)

		return response
	}

	return &GuardianTaskResponse{
		Executed: false,
	}
}

func (instance *GuardianTask) runJavascript() *GuardianTaskResponse {
	if nil != instance && nil == instance.program {
		program, err := scripting.NewProgram(instance.workspacePath, instance.uid, instance.programFile,
			instance.Silent, instance.variables, instance.modules, instance.logger)
		if nil != err {
			instance.chanResponse <- &GuardianTaskResponse{
				Err: err,
			}
		}
		if nil != program {
			instance.program = program
			if !instance.KeepAlive() {
				defer instance.close() // close and finalize
			}

			response := &GuardianTaskResponse{
				Executed: true,
				StartAt:  time.Now(),
			}

			// add some context values
			program.Context().Set("schedule", instance.schedule)
			value, functionFound, err := program.Run(instance.FunctionName, instance.FunctionArgs...)

			response.ExecutedFunction = functionFound
			response.Value = value
			response.Err = err
			response.EndAt = time.Now()
			response.Elapsed = response.EndAt.Sub(response.StartAt)

			return response
		}
	}
	return &GuardianTaskResponse{
		Executed: false,
	}
}

func (instance *GuardianTask) close() {
	if nil != instance {
		if nil != instance.program {
			instance.program.Close()
		}
		instance.program = nil // REMOVE
		instance.schedule = nil
	}
}
