package webserver_authentication

import (
	"bitbucket.org/lygo/lygo_commons/lygo_conv"
	"bitbucket.org/lygo/lygo_commons/lygo_csv"
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_commons/lygo_reflect"
	"bitbucket.org/lygo/lygo_ext_auth0"
	"bitbucket.org/lygo/lygo_ext_dbal"
	"bitbucket.org/lygo/lygo_ext_dbal/dbal_drivers"
	"errors"
	"strings"
	"time"
)

// ---------------------------------------------------------------------------------------------------------------------
//	t y p e
// ---------------------------------------------------------------------------------------------------------------------

type AppWebserverAuthenticationDatabase struct {
	config     *lygo_ext_auth0.Auth0ConfigStorage
	collection string
	database   dbal_drivers.IDatabase
	err        error
}

func NewAppWebserverAuthenticationDatabase(config *lygo_ext_auth0.Auth0ConfigStorage, collection string) *AppWebserverAuthenticationDatabase {
	instance := new(AppWebserverAuthenticationDatabase)
	instance.collection = collection
	if len(instance.collection) == 0 {
		instance.collection = "users"
	}
	instance.database, instance.err = lygo_ext_dbal.NewDatabase(config.Driver, config.Dsn)

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *AppWebserverAuthenticationDatabase) UserEmailExists(fieldName, fieldValue string) (bool, error) {
	if nil != instance.err {
		return false, instance.err
	}
	data, err := instance.database.Find(instance.collection, fieldName, fieldValue)
	if nil != err {
		return false, err
	}
	array := lygo_conv.ToArray(data)
	if len(array) > 0 {
		return true, nil
	}
	return false, nil
}

func (instance *AppWebserverAuthenticationDatabase) CreateNewUser(authResponse *lygo_ext_auth0.Auth0Response, payload map[string]interface{}) (map[string]interface{}, error) {
	if nil != instance.err {
		return nil, instance.err
	}

	now := time.Now().Unix()
	user := payload
	user["auth_id"] = authResponse.ItemId
	user["confirm_token"] = authResponse.ConfirmToken
	user["member_since"] = now
	user["last_password_change"] = now
	user["account_verified"] = false
	user["role"] = 0

	return instance.database.Upsert(instance.collection, user)
}

func (instance *AppWebserverAuthenticationDatabase) UpdateUser(user map[string]interface{}, passwordChanged bool) (map[string]interface{}, error) {
	if nil != instance.err {
		return nil, instance.err
	}
	if passwordChanged {
		user["last_password_change"] = time.Now().Unix()
	}
	return instance.database.Upsert(instance.collection, user)
}

func (instance *AppWebserverAuthenticationDatabase) RemoveUser(key string) (map[string]interface{}, error) {
	if nil != instance.err {
		return nil, instance.err
	}
	user, err := instance.database.Get(instance.collection, key)
	if nil != err {
		return nil, err
	}
	err = instance.database.Remove(instance.collection, key)
	return user, err
}

func (instance *AppWebserverAuthenticationDatabase) GetUser(field string, value interface{}) (map[string]interface{}, error) {
	data, err := instance.database.Find(instance.collection, field, value)
	if nil != err {
		return nil, err
	}
	array := lygo_conv.ToArray(data)
	if len(array) > 0 {
		user := array[0]
		if v, b := user.(map[string]interface{}); b {
			return v, nil
		}
	}
	return nil, nil
}

func (instance *AppWebserverAuthenticationDatabase) GetUserConfirmToken(field string, value interface{}) (string, error) {
	data, err := instance.database.Find(instance.collection, field, value)
	if nil != err {
		return "", err
	}
	array := lygo_conv.ToArray(data)
	if len(array) > 0 {
		user := array[0]
		return lygo_reflect.GetString(user, "confirm_token"), nil
	}
	return "", nil
}

func (instance *AppWebserverAuthenticationDatabase) ConfirmUser(authId string) (map[string]interface{}, error) {
	user, err := instance.GetUser("auth_id", authId)
	if nil != err {
		return nil, err
	}
	user["account_verified"] = true
	user["confirm_token"] = ""
	return instance.database.Upsert(instance.collection, user)
}

func (instance *AppWebserverAuthenticationDatabase) GetUserNotVerified(days int) []map[string]interface{} {
	response := make([]map[string]interface{}, 0)
	if nil == instance.err && days > 0 {
		_ = instance.database.ForEach(instance.collection, func(m map[string]interface{}) bool {
			accountVerified := lygo_reflect.GetBool(m, "account_verified")
			if !accountVerified {
				memberSince := int64(lygo_reflect.GetInt(m, "member_since"))
				memberSinceDays := time.Now().Sub(time.Unix(memberSince, 0)).Hours() / 24
				if int(memberSinceDays) > days {
					response = append(response, m)
				}
			}
			return false
		})
	}
	return response
}

func (instance *AppWebserverAuthenticationDatabase) ParseUserFromFile(file string) ([]map[string]interface{}, error) {
	switch strings.ToLower(lygo_paths.ExtensionName(file)) {
	case "json":
		return instance.parseJSON(file)
	case "csv", "txt":
		return instance.parseTEXT(file)
	}
	return nil, errors.New("filetype_not_supported")
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *AppWebserverAuthenticationDatabase) parseTEXT(file string) ([]map[string]interface{}, error) {
	response := make([]map[string]interface{}, 0)
	text, err := lygo_io.ReadTextFromFile(file)
	if nil != err {
		return nil, err
	}
	if len(text) > 0 {
		options := lygo_csv.NewCsvOptionsDefaults()
		data, err := lygo_csv.ReadAll(text, options)
		if nil != err {
			return nil, err
		}
		for _, item := range data {
			response = append(response, lygo_conv.ToMap(item))
		}
	}
	return response, nil
}

func (instance *AppWebserverAuthenticationDatabase) parseJSON(file string) ([]map[string]interface{}, error) {
	response := make([]map[string]interface{}, 0)
	bytes, err := lygo_io.ReadBytesFromFile(file)
	if nil != err {
		return nil, err
	}
	if len(bytes) > 0 {
		if string(bytes[0]) == "{" {
			var m map[string]interface{}
			err = lygo_json.Read(bytes, &m)
			if nil != err {
				return nil, err
			}
			response = append(response, m)
		} else {
			var a []map[string]interface{}
			err = lygo_json.Read(bytes, &a)
			if nil != err {
				return nil, err
			}
			response = append(response, a...)
		}
	}
	return response, nil
}
