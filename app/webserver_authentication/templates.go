package webserver_authentication

import (
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"github.com/cbroglie/mustache"
)

// ---------------------------------------------------------------------------------------------------------------------
//	t y p e
// ---------------------------------------------------------------------------------------------------------------------

type AppWebserverAuthenticationTemplates struct {
	dirWork      string
	dirTemplates string
}

func NewAppWebserverAuthenticationTemplates(dirWork, dirTemplates string) *AppWebserverAuthenticationTemplates {
	instance := new(AppWebserverAuthenticationTemplates)
	instance.dirWork = dirWork
	instance.dirTemplates = dirTemplates

	// initialize directories
	if len(instance.dirTemplates) == 0 {
		instance.dirTemplates = "./templates"
	}
	instance.dirTemplates = lygo_paths.Absolutize(instance.dirTemplates, instance.dirWork)
	_ = lygo_paths.Mkdir(instance.dirTemplates + lygo_paths.OS_PATH_SEPARATOR)

	// creates sub dirs
	_ = lygo_paths.Mkdir(lygo_paths.Concat(instance.dirTemplates, "email") + lygo_paths.OS_PATH_SEPARATOR)
	_ = lygo_paths.Mkdir(lygo_paths.Concat(instance.dirTemplates, "sms") + lygo_paths.OS_PATH_SEPARATOR)
	_ = lygo_paths.Mkdir(lygo_paths.Concat(instance.dirTemplates, "html") + lygo_paths.OS_PATH_SEPARATOR)



	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *AppWebserverAuthenticationTemplates) Check() ([]string, []error){
	// download templates if do not exists
	downloader := NewAppWebserverAuthenticationTemplatesGenerator(instance.dirTemplates)
	return downloader.Check()
}

// sample: Render("email", "verify.html", {"username":"Mario"})
func (instance *AppWebserverAuthenticationTemplates) Render(dir, name string, data map[string]interface{}) (string, error) {
	content, err := instance.Get(dir, name)
	if nil != err {
		return "", err
	}
	return mustache.Render(content, data)
}

// sample: Get("email", "verify.html")
func (instance *AppWebserverAuthenticationTemplates) Get(dir, name string) (string, error) {
	filename := lygo_paths.Concat(instance.dirTemplates, dir, name)
	return lygo_io.ReadTextFromFile(filename)
}
