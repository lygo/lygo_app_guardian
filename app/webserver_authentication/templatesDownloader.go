package webserver_authentication

import (
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
)

/**
Download templates from github
*/

// ---------------------------------------------------------------------------------------------------------------------
// 	t y p e
// ---------------------------------------------------------------------------------------------------------------------

type AppWebserverAuthenticationTemplatesDownloader struct {
	dirTemplates string
	actions      []*lygo_io.DownloaderAction
}

func NewAppWebserverAuthenticationTemplatesGenerator(dirTemplates string) *AppWebserverAuthenticationTemplatesDownloader {
	instance := new(AppWebserverAuthenticationTemplatesDownloader)
	instance.dirTemplates = dirTemplates

	instance.init()

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
// 	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *AppWebserverAuthenticationTemplatesDownloader) Check() ([]string, []error) {
	// download
	session := lygo_io.NewDownloadSession(instance.actions)
	return session.DownloadAll(false)
}

// ---------------------------------------------------------------------------------------------------------------------
// 	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *AppWebserverAuthenticationTemplatesDownloader) init() {
	// email
	instance.actions = append(instance.actions, lygo_io.NewAction("",
		"https://bitbucket.org/lygo/lygo_app_guardian/raw/master/_guardian/webserver/templates/email/password-reset.html.htm", "",
		lygo_paths.ConcatDir(instance.dirTemplates, "email")))
	instance.actions = append(instance.actions, lygo_io.NewAction("",
		"https://bitbucket.org/lygo/lygo_app_guardian/raw/master/_guardian/webserver/templates/email/password-reset.text.txt", "",
		lygo_paths.ConcatDir(instance.dirTemplates, "email")))
	instance.actions = append(instance.actions, lygo_io.NewAction("",
		"https://bitbucket.org/lygo/lygo_app_guardian/raw/master/_guardian/webserver/templates/email/password-reset.subject.txt", "",
		lygo_paths.ConcatDir(instance.dirTemplates, "email")))
	instance.actions = append(instance.actions, lygo_io.NewAction("",
		"https://bitbucket.org/lygo/lygo_app_guardian/raw/master/_guardian/webserver/templates/email/verify.html.htm", "",
		lygo_paths.ConcatDir(instance.dirTemplates, "email")))
	instance.actions = append(instance.actions, lygo_io.NewAction("",
		"https://bitbucket.org/lygo/lygo_app_guardian/raw/master/_guardian/webserver/templates/email/verify.text.txt", "",
		lygo_paths.ConcatDir(instance.dirTemplates, "email")))
	instance.actions = append(instance.actions, lygo_io.NewAction("",
		"https://bitbucket.org/lygo/lygo_app_guardian/raw/master/_guardian/webserver/templates/email/verify.subject.txt", "",
		lygo_paths.ConcatDir(instance.dirTemplates, "email")))
	instance.actions = append(instance.actions, lygo_io.NewAction("",
		"https://bitbucket.org/lygo/lygo_app_guardian/raw/master/_guardian/webserver/templates/email/welcome.html.htm", "",
		lygo_paths.ConcatDir(instance.dirTemplates, "email")))
	instance.actions = append(instance.actions, lygo_io.NewAction("",
		"https://bitbucket.org/lygo/lygo_app_guardian/raw/master/_guardian/webserver/templates/email/welcome.text.txt", "",
		lygo_paths.ConcatDir(instance.dirTemplates, "email")))
	instance.actions = append(instance.actions, lygo_io.NewAction("",
		"https://bitbucket.org/lygo/lygo_app_guardian/raw/master/_guardian/webserver/templates/email/welcome.subject.txt", "",
		lygo_paths.ConcatDir(instance.dirTemplates, "email")))

	// sms
	instance.actions = append(instance.actions, lygo_io.NewAction("",
		"https://bitbucket.org/lygo/lygo_app_guardian/raw/master/_guardian/webserver/templates/sms/verify.text.txt", "",
		lygo_paths.ConcatDir(instance.dirTemplates, "sms")))

	// html
	instance.actions = append(instance.actions, lygo_io.NewAction("",
		"https://bitbucket.org/lygo/lygo_app_guardian/raw/master/_guardian/webserver/templates/html/error.html", "",
		lygo_paths.ConcatDir(instance.dirTemplates, "html")))
	instance.actions = append(instance.actions, lygo_io.NewAction("",
		"https://bitbucket.org/lygo/lygo_app_guardian/raw/master/_guardian/webserver/templates/html/password-reset.html", "",
		lygo_paths.ConcatDir(instance.dirTemplates, "html")))
	instance.actions = append(instance.actions, lygo_io.NewAction("",
		"https://bitbucket.org/lygo/lygo_app_guardian/raw/master/_guardian/webserver/templates/html/verified.html", "",
		lygo_paths.ConcatDir(instance.dirTemplates, "html")))

}
