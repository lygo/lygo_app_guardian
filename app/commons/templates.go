package commons

import (
	"bytes"
	"strings"
	"text/template"
)

const TplFileSettings = `
{
  "silent": true,
  "log-level": "{{.Mode}}",
  "actions": []
}
`

const TplFileWebserver = `
{
  "enabled": false,
  "http": {
    "root": "./webserver",
    "server": {
      "enable_request_id": true,
      "prefork": false
    },
    "hosts": [
      {
        "addr": ":80",
        "tls": false,
        "websocket": {
          "enabled": true
        }
      },
      {
        "addr": ":443",
        "tls": true,
        "ssl_cert": "./cert/ssl-cert.pem",
        "ssl_key": "./cert/ssl-cert.key",
        "websocket": {
          "enabled": true
        }
      }
    ],
    "static": [
      {
        "enabled": true,
        "prefix": "/",
        "root": "./www",
        "index": "",
        "compress": true
      }
    ],
    "compression": {
      "enabled": false,
      "level": 0
    },
    "limiter": {
      "enabled": false,
      "timeout": 30,
      "max": 10
    },
    "CORS": {
      "enabled": true
    }
  },
  "initialize": [
    {
      "uid": "webserver_init",
      "program": "./programs/webserver/tasks/init/init.js",
      "variables": {
        "PORT": 10001
      }
    }
  ],
  "authentication": {
    "enabled": false,
    "app-base-url": "https://gianangelogeminiani.me/",
    "api-base-url": "http://localhost/",
    "dir-templates": "./webserver/templates",
    "security": {
      "app-token": "",
      "authorization": {
        "type": "bearer",
        "value": "7uo5dc8473ywdrr59fqgargipo"
      }
    },
    "db-users-collection": "users",
    "postman": {
      "payload": {
        "owner_name": "SENDER NAME",
        "owner_sign": "Best regards, The Team",
        "some-data": "Put here default data to pass at templates"
      },
      "config-mail": {
        "host": "smtp.mailtrap.io",
        "port": 2525,
        "secure": false,
        "user": "xxxx",
        "pass": "xxxx",
        "from": "no replay <info@email.com>"
      },
      "config-sms": {
        "enabled": true,
        "auto-short-url": true,
        "providers": {
          "smshosting": {
            "method": "GET",
            "endpoint": "https://api.smshosting.it/rest/api/smart/sms/send?authKey={{auth-key}}&authSecret={{auth-secret}}&text={{message}}&to={{to}}&from={{from}}",
            "params": {
              "auth-key": "",
              "auth-secret": "",
              "message": "",
              "to": "",
              "from": "GUARDIAN"
            },
            "headers": {}
          }
        }
      }
    },
    "routing": {
      "sign-up": {
        "method": "POST",
        "endpoint": "/api/guardian/auth/sign-up",
        "params-required": [
          "email",
          "password",
          "firstname",
          "lastname",
          "privacy_policy_consent"
        ],
        "params": [
          "email",
          "password",
          "firstname",
          "lastname",
          "privacy_policy_consent"
        ]
      },
      "remove": {
        "method": "POST",
        "endpoint": "/api/guardian/auth/remove",
        "params-required": [
          "_key"
        ],
        "params": [
          "_key"
        ]
      }
    }
  },
  "routing": [
    {
      "method": "ALL",
      "endpoint": "/api/v4/test/*",
      "action": {
        "uid": "endpoint_test",
        "program": "./programs/webhandlers/test_api.js",
        "variables": {
          "PORT": 10001,
          "authorization": {
            "type": "none",
            "value": ""
          },
          "database": {
            "driver": "arango",
            "dsn": "root:password@tcp(localhost:8529)/test)"
          }
        }
      }
    }
  ]
}
`

const TplFileUpdater = `
{
  "enabled": false,
  "updaters": [
    {
      "uid": "nio_server",
      "version_file": "http://localhost/scripts/nio_server/version.txt",
      "package_files": [
        {
          "file": "http://localhost/scripts/nio_server/nio_server.js",
          "target": "$dir_program"
        }
      ],
      "scheduled_updates": [
        {
          "uid": "every_5_seconds",
          "start_at": "",
          "timeline": "second:5"
        }
      ]
    }
  ]
}
`

const TplFileSecure = `
{
  "secrets": {
    "auth": "this-is-token-to-authenticate",
    "access": "hsdfuhksdhf5435khjsd",
    "refresh": "hsdfuhqswe34qwksdhfkhjsd"
  },
  "cache-storage": {
    "driver": "arango",
    "dsn": "root:root@tcp(localhost:8529)/test"
  },
  "auth-storage": {
    "driver": "arango",
    "dsn": "root:root@tcp(localhost:8529)/test"
  }
}
`

const TplFileDevelopment = `
{
  "enabled": false,
  "http": {
    "root": "./_development/_webui",
    "server": {
      "enable_request_id": true,
      "prefork": false
    },
    "hosts": [
      {
        "addr": ":9090",
        "tls": false,
        "websocket": {
          "enabled": true
        }
      },
      {
        "addr": ":9091",
        "tls": true,
        "ssl_cert": "./cert/ssl-cert.pem",
        "ssl_key": "./cert/ssl-cert.key",
        "websocket": {
          "enabled": true
        }
      }
    ],
    "static": [
      {
        "enabled": true,
        "prefix": "/",
        "root": "./www",
        "index": "",
        "compress": true
      }
    ],
    "compression": {
      "enabled": false,
      "level": 0
    },
    "limiter": {
      "enabled": false,
      "timeout": 30,
      "max": 10
    },
    "CORS": {
      "enabled": true
    }
  },
  "authorization": {
    "type": "base",
    "value": "MTIzOmFiYw=="
  }
}
`


func MergeTpl(text string, data interface{}) (string, error) {
	if nil==data{
		data = struct {}{}
	}
	buff := bytes.NewBufferString("")
	t := template.Must(template.New("template").Parse(strings.Trim(text, "\n")))
	err := t.Execute(buff, data)
	if nil != err {
		return "", err
	}
	return buff.String(), err
}