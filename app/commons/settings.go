package commons

import (
	"bitbucket.org/lygo/lygo_commons/lygo_conv"
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_scheduler"
	lygo_updater "bitbucket.org/lygo/lygo_updater/src"
	"fmt"
	"github.com/cbroglie/mustache"
	"strings"
)

// ---------------------------------------------------------------------------------------------------------------------
//	Action
// ---------------------------------------------------------------------------------------------------------------------

type Action struct {
	lygo_scheduler.Schedule
	Enabled   interface{}            `json:"enabled"`
	KeepAlive bool                   `json:"keep_alive"`
	Program   string                 `json:"program"`
	Variables map[string]interface{} `json:"variables"`
}

func (instance *Action) GetRuntimeName() string {
	if nil != instance {
		return strings.ToLower(lygo_paths.ExtensionName(instance.Program))
	}
	return ""
}

func (instance *Action) IsEnabled() bool {
	if nil != instance {
		if nil != instance.Enabled {
			return lygo_conv.ToBool(instance.Enabled)
		}
	}
	return true
}

// ---------------------------------------------------------------------------------------------------------------------
//	ActionVariables
// ---------------------------------------------------------------------------------------------------------------------

type ActionVariableDatabase struct {
	Driver string `json:"driver"`
	Dsn    string `json:"dsn"`
}

type ActionVariableAuthorization struct {
	Type  string `json:"type"`
	Value string `json:"value"`
}

type ActionVariables struct {
	Database      interface{}                  `json:"database"`
	Authorization *ActionVariableAuthorization `json:"authorization"`
}

func NewActionVariablesFromMap(m map[string]interface{}) *ActionVariables {
	var response ActionVariables
	err := lygo_json.Read(lygo_json.Stringify(m), &response)
	if nil != err {
		fmt.Println(err)
	}
	return &response
}

// ---------------------------------------------------------------------------------------------------------------------
//	GuardianUpdaterSettings
// ---------------------------------------------------------------------------------------------------------------------

type GuardianUpdaterSettings struct {
	Enable   bool                     `json:"enabled"`
	Updaters []*lygo_updater.Settings `json:"updaters"`
}

// ---------------------------------------------------------------------------------------------------------------------
//	GuardianSettings
// ---------------------------------------------------------------------------------------------------------------------

type GuardianSettings struct {
	Silent   bool      `json:"silent"`
	LogLevel string    `json:"log-level"`
	StopCmd  string    `json:"stop-cmd"`
	Actions  []*Action `json:"actions"`
}

func NewGuardianSettings(mode string) (*GuardianSettings, error) {
	settings := new(GuardianSettings)
	err := ensureSettingsFilesExists(mode)
	if nil != err {
		return settings, err
	}
	path := lygo_paths.WorkspacePath("settings." + mode + ".json")
	text, err := lygo_io.ReadTextFromFile(path)
	if nil != err {
		return settings, err
	}
	context := make(map[string]interface{})
	context["workspace"] = lygo_paths.GetWorkspacePath()
	text, err = mustache.Render(text, context)
	if nil != err {
		return settings, err
	}
	err = lygo_json.Read(text, &settings)
	return settings, err
}

func NewGuardianEmptySettings() *GuardianSettings {
	instance := new(GuardianSettings)
	instance.LogLevel = "debug"
	instance.Silent = false
	instance.Actions = make([]*Action, 0)
	return instance
}

func NewGuardianSettingsFromData(programsRoot string, data []map[string]string) *GuardianSettings {
	settings := NewGuardianEmptySettings()
	for _, m := range data {
		program := m["program"]
		timeline := m["timeline"]
		start_at := m["start_at"]
		uid := m["uid"]
		if len(uid) == 0 {
			uid = lygo_paths.FileName(program, false)
		}

		settings.Actions = append(settings.Actions, &Action{
			Schedule: lygo_scheduler.Schedule{
				Uid:      uid,
				StartAt:  start_at,
				Timeline: timeline,
			},
			Program: lygo_paths.Concat(programsRoot, program),
		})
	}
	return settings
}

// ---------------------------------------------------------------------------------------------------------------------
//		p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *GuardianSettings) Parse(text string) error {
	return lygo_json.Read(text, &instance)
}

func (instance *GuardianSettings) String() string {
	return lygo_json.Stringify(instance)
}

// ---------------------------------------------------------------------------------------------------------------------
//		p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func ensureSettingsFilesExists(mode string) error {
	err := writeFile(lygo_paths.WorkspacePath("settings."+mode+".json"), TplFileSettings, map[string]interface{}{"Mode": mode})
	if nil != err {
		return err
	}

	err = writeFile(lygo_paths.WorkspacePath("webserver."+mode+".json"), TplFileWebserver, map[string]interface{}{"Mode": mode})
	if nil != err {
		return err
	}

	err = writeFile(lygo_paths.WorkspacePath("updater."+mode+".json"), TplFileUpdater, map[string]interface{}{"Mode": mode})
	if nil != err {
		return err
	}

	err = writeFile(lygo_paths.WorkspacePath("secure."+mode+".json"), TplFileSecure, map[string]interface{}{"Mode": mode})
	if nil != err {
		return err
	}

	return nil
}

func writeFile(filename string, tpl string, data interface{}) error {
	if b, _ := lygo_paths.Exists(filename); !b {
		text, err := MergeTpl(tpl, data)
		if nil != err {
			return err
		}
		_, err = lygo_io.WriteTextToFile(text, filename)
		if nil != err {
			return err
		}
	}
	return nil
}
