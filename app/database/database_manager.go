package database

import (
	"bitbucket.org/lygo/lygo_app_guardian/app/commons"
	"bitbucket.org/lygo/lygo_commons/lygo_reflect"
	"bitbucket.org/lygo/lygo_commons/lygo_regex"
	"bitbucket.org/lygo/lygo_ext_dbal/dbal_drivers"
	"strings"
)

// ---------------------------------------------------------------------------------------------------------------------
//	t y p e
// ---------------------------------------------------------------------------------------------------------------------

type DatabaseManager struct {
	logger *commons.Logger
	cache  *dbal_drivers.CacheManager
}

// ---------------------------------------------------------------------------------------------------------------------
//	c o n s t r u c t o r
// ---------------------------------------------------------------------------------------------------------------------

func NewDatabaseManager(logger *commons.Logger) *DatabaseManager {
	instance := new(DatabaseManager)
	instance.logger = logger
	instance.cache = dbal_drivers.Cache()
	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *DatabaseManager) Open() {

}

func (instance *DatabaseManager) Close() {
	instance.cache.Close()
}

func (instance *DatabaseManager) Clear() {
	instance.cache.Clear()
}

/**
"database": {
	"driver": "arango",
	"dsn": "root:1234567890@tcp(localhost:8529)/my-database)"
}
*/
func (instance *DatabaseManager) Get(settings map[string]interface{}) (dbal_drivers.IDatabase, error) {
	driver := lygo_reflect.GetString(settings, "driver")
	dsn := lygo_reflect.GetString(settings, "dsn")
	if len(driver) == 0 || len(dsn) == 0 {
		return nil, commons.InvalidConfigurationError
	}
	return instance.cache.Get(driver, dsn)
}

func (instance *DatabaseManager) Remove(uid string) {
	instance.cache.Remove(uid)
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

// ---------------------------------------------------------------------------------------------------------------------
//	S T A T I C
// ---------------------------------------------------------------------------------------------------------------------

func QueryParamNames(query string) []string {
	query = strings.ReplaceAll(query, ";", " ;")
	query += " "
	return lygo_regex.TextBetweenStrings(query, "@", " ")
}
