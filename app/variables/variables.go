package variables

import (
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_commons/lygo_reflect"
)

// ---------------------------------------------------------------------------------------------------------------------
//	t y p e
// ---------------------------------------------------------------------------------------------------------------------

type AppVariables struct {
	data map[string]interface{}
}

func NewVariables(mode string) *AppVariables {
	instance := new(AppVariables)
	instance.data = make(map[string]interface{})
	m, err := load(mode)
	if nil == err {
		instance.PutAll(m)
	}

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *AppVariables) String() string {
	return lygo_json.Stringify(instance.data)
}

func (instance *AppVariables) GoString() string {
	return lygo_json.Stringify(instance.data)
}

func (instance *AppVariables) GetAll() map[string]interface{} {
	if nil != instance {
		return instance.data
	}
	return nil
}

func (instance *AppVariables) Get(key string) interface{} {
	if nil != instance {
		if v, b := instance.data[key]; b {
			return v
		}
	}
	return nil
}

func (instance *AppVariables) Put(key string, value interface{}) map[string]interface{} {
	if nil != instance {
		// instance.data[key] = value
		instance.put(key, value, instance.data)
		return instance.data
	}
	return nil
}

func (instance *AppVariables) PutAll(data map[string]interface{}) *AppVariables {
	if nil != instance {
		for k, v := range data {
			instance.Put(k, v)
		}
		return instance
	}
	return nil
}

func (instance *AppVariables) Extend(data map[string]interface{}) map[string]interface{} {
	response := map[string]interface{}{}
	for k, v := range data {
		response[k] = v
	}
	lygo_reflect.ExtendMap(response, instance.data, false)
	return response
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *AppVariables) put(key string, value interface{}, target map[string]interface{}) {
	if nil != target {
		target[key] = value
	}
}

func load(mode string) (map[string]interface{}, error) {
	path := lygo_paths.WorkspacePath("variables." + mode + ".json")
	if b, _ := lygo_paths.Exists(path); !b {
		_, _ = lygo_io.WriteTextToFile("{}", path)
	}
	m, err := lygo_json.ReadMapFromFile(path)
	if nil != err {
		return nil, err
	}
	return m, err
}
