package ui

import (
	"bitbucket.org/lygo/lygo_app_guardian/app/commons"
	"bitbucket.org/lygo/lygo_app_guardian/app/webserver"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
)

// ---------------------------------------------------------------------------------------------------------------------
//		t y p e
// ---------------------------------------------------------------------------------------------------------------------

type AppUI struct {
	mode      string
	dirWork   string // guardian workspace
	logger    *commons.Logger
	webserver *webserver.AppWebserver
	launcher  *AppUILauncher

	enabled  bool
	settings *AppUISettings
	url      string
}

func NewAppUI(mode, dirWork string, logger *commons.Logger, webserver *webserver.AppWebserver) *AppUI {
	instance := new(AppUI)
	instance.mode = mode
	instance.dirWork = dirWork
	instance.logger = logger
	instance.webserver = webserver
	instance.launcher = NewAppUILauncher(dirWork)

	instance.init()

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *AppUI) Start() {
	if nil != instance && instance.enabled {
		instance.open()
	}
}

func (instance *AppUI) Stop() {
	if nil != instance && instance.enabled {
		instance.close()
	}
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *AppUI) init() {
	settings, err := LoadSettings(instance.mode)
	if nil == err {
		instance.enabled = settings.Enabled
		instance.settings = settings
	}
}

func (instance *AppUI) open() {
	if nil != instance.webserver {
		instance.url = lygo_paths.Concat(instance.webserver.LocalUrl(), instance.settings.Home)
		_ = instance.launcher.Open(instance.url, instance.settings.BrowserMode)
	}
}

func (instance *AppUI) close() {

}

// ---------------------------------------------------------------------------------------------------------------------
//		S T A T I C
// ---------------------------------------------------------------------------------------------------------------------
