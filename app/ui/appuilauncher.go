package ui

import (
	"bitbucket.org/lygo/lygo_commons/lygo_exec"
	"bitbucket.org/lygo/lygo_commons/lygo_exec/console_program/chrome"
)

type AppUILauncher struct {
	dirWork string
	command *chrome.Command
}

func NewAppUILauncher(dirWork string) *AppUILauncher {
	instance := new(AppUILauncher)
	instance.dirWork = dirWork

	return instance
}

func (instance *AppUILauncher) Open(url string, browserMode bool) (err error) {
	instance.command = chrome.NewCommand()
	instance.command.SetDir(instance.dirWork)
	instance.command.Params().AppMode = !browserMode
	err = instance.command.Open(url) // try open Chrome in App Mode
	if nil != err {
		instance.command = nil // reset command that failed
		// launch a browser
		err = lygo_exec.Open(url)
	}
	return
}
