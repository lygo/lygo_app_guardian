package ui

import (
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
)

type AppUISettings struct {
	Enabled     bool   `json:"enabled"`
	Home        string `json:"home"`
	BrowserMode bool   `json:"browser-mode"`
}

func LoadSettings(mode string) (*AppUISettings, error) {
	settings := new(AppUISettings)
	path := lygo_paths.WorkspacePath("ui." + mode + ".json")
	text, err := lygo_io.ReadTextFromFile(path)
	if nil != err {
		return settings, err
	}
	err = lygo_json.Read(text, &settings)
	return settings, err
}
