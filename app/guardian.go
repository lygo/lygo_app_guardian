package app

import (
	"bitbucket.org/lygo/lygo_app_guardian/app/commons"
	"bitbucket.org/lygo/lygo_app_guardian/app/secure"
	"bitbucket.org/lygo/lygo_app_guardian/app/tasks"
	"bitbucket.org/lygo/lygo_app_guardian/app/ui"
	"bitbucket.org/lygo/lygo_app_guardian/app/updater"
	"bitbucket.org/lygo/lygo_app_guardian/app/variables"
	"bitbucket.org/lygo/lygo_app_guardian/app/webserver"
	"bitbucket.org/lygo/lygo_commons/lygo_errors"
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_events"
	"bitbucket.org/lygo/lygo_ext_scripting"
	"bitbucket.org/lygo/lygo_scheduler"
	"errors"
	"path/filepath"
	"time"
)

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

type Guardian struct {
	root            string
	dirStart        string
	dirApp          string
	dirWork         string
	logger          *commons.Logger
	settings        *commons.GuardianSettings
	mode            string
	stopChan        chan bool
	events          *lygo_events.Emitter
	scheduler       *lygo_scheduler.Scheduler
	modules         *lygo_ext_scripting.ModuleRegistry
	secureManager   *secure.AppSecure
	variableManager *variables.AppVariables
	taskManager     *tasks.TaskManager
	updateManager   *updater.UpdateManager
	stopMonitor     *StopMonitor
	webserver       *webserver.AppWebserver
	pathVariables   map[string]string
	ui              *ui.AppUI
}

//----------------------------------------------------------------------------------------------------------------------
//	c o n s t r u c t o r
//----------------------------------------------------------------------------------------------------------------------

func NewGuardian(mode string, settings *commons.GuardianSettings) (instance *Guardian, err error) {
	instance = new(Guardian)

	instance.dirStart = lygo_paths.GetWorkspace(commons.WpDirStart).GetPath()
	instance.dirApp = lygo_paths.GetWorkspace(commons.WpDirApp).GetPath()
	instance.dirWork = lygo_paths.GetWorkspace(commons.WpDirWork).GetPath()
	instance.root = filepath.Dir(instance.dirWork)

	instance.pathVariables = make(map[string]string)
	instance.pathVariables[commons.PathDirStart] = instance.dirStart
	instance.pathVariables[commons.PathDirApp] = instance.dirApp
	instance.pathVariables[commons.PathDirWork] = instance.dirWork
	instance.pathVariables[commons.PathDirPrograms] = filepath.Join(instance.dirWork, "programs")
	instance.pathVariables[commons.PathDirModules] = filepath.Join(instance.dirWork, "modules")

	instance.logger = commons.NewLogger(mode)
	if nil == settings {
		instance.settings, err = commons.NewGuardianSettings(mode)
	} else {
		instance.settings = settings
	}
	instance.mode = mode
	instance.stopChan = make(chan bool, 1)
	instance.events = lygo_events.NewEmitter()
	instance.modules = lygo_ext_scripting.NewModuleRegistry(loadModule)
	// scheduler
	instance.scheduler = lygo_scheduler.NewScheduler()
	instance.scheduler.OnError(instance.handleSchedulerError)
	instance.scheduler.OnSchedule(instance.handleSchedulerEvent)

	if len(instance.settings.LogLevel) > 0 {
		instance.logger.SetLevel(instance.settings.LogLevel)
	}

	// important logger and controllers
	instance.variableManager = variables.NewVariables(mode)
	instance.variableManager.Put(commons.VarDirStart, instance.dirStart)
	instance.variableManager.Put(commons.VarDirApp, instance.dirApp)
	instance.variableManager.Put(commons.VarDirWork, instance.dirWork)
	instance.variableManager.Put(commons.VarDirPrograms, filepath.Join(instance.dirWork, "programs"))
	instance.variableManager.Put(commons.VarDirModules, filepath.Join(instance.dirWork, "modules"))
	instance.variableManager.Put(commons.VarFilename, "")
	instance.variableManager.Put(commons.VarDirname, "")
	instance.variableManager.Put(commons.VarUID, "")

	instance.secureManager = secure.NewAppSecure(mode, instance.dirWork, instance.logger)
	instance.taskManager = tasks.NewTaskManager(instance.dirWork, instance.logger, instance.secureManager,
		instance.variableManager, instance.settings, instance.modules)
	instance.updateManager = updater.NewUpdateManager(mode, instance.dirWork, instance.pathVariables, instance.logger,
		instance.settings, instance.taskManager.StopTask, instance.taskManager.Stop)
	instance.webserver = webserver.NewAppWebserver(mode, instance.dirWork, instance.logger, instance.secureManager,
		instance.taskManager)
	instance.ui = ui.NewAppUI(mode, instance.dirWork, instance.logger, instance.webserver)

	// stop monitor
	instance.stopMonitor = NewStopMonitor(instance.dirWork, instance.settings.StopCmd, instance.logger, instance.events)

	// handle internal events
	instance.events.On(commons.EventOnDoStop, instance.handleDoStopEvent)

	return instance, err
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *Guardian) GetLogger() *commons.Logger {
	if nil != instance {
		return instance.logger
	}
	return nil
}

func (instance *Guardian) Start() (err error) {
	if nil != instance {
		// start scheduler
		err = instance.start()

		return err
	}
	return commons.PanicSystemError
}

func (instance *Guardian) Stop() (err error) {
	if nil != instance {
		// start scheduler
		err = instance.stop()
		return
	}
	return commons.PanicSystemError
}

func (instance *Guardian) Wait() {
	// wait exit
	<-instance.stopChan
	// reset channel
	instance.stopChan = make(chan bool, 1)
}

func (instance *Guardian) WaitTimeout(duration time.Duration) {
	go func() {
		time.Sleep(duration)
		_ = instance.Stop()
	}()
	instance.Wait()
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *Guardian) start() error {
	if nil != instance {
		if nil != instance.settings {
			if nil != instance.settings.Actions {

				// load actions in scheduler
				for _, action := range instance.settings.Actions {
					if nil != action {
						// add schedule item and pass *Action as argument
						instance.scheduler.AddScheduleByJson(lygo_json.Stringify(action), action)
					}
				}

				_ = instance.secureManager.Start()
				instance.scheduler.Start()
				instance.updateManager.Start()
				instance.webserver.Start()
				instance.stopMonitor.Start()
				instance.ui.Start()

				return nil
			}
			return lygo_errors.Prefix(commons.InvalidConfigurationError, "Missing Actions: ")
		}
		return lygo_errors.Prefix(commons.InvalidConfigurationError, "Missing Settings: ")
	}
	return commons.PanicSystemError
}

func (instance *Guardian) stop() error {
	if nil != instance {

		if nil != instance.stopChan {
			instance.ui.Stop()
			instance.stopMonitor.Stop()
			instance.webserver.Stop()
			instance.scheduler.Stop()
			instance.taskManager.Stop()
			instance.updateManager.Stop()
			instance.secureManager.Stop()

			instance.stopChan <- true
			instance.stopChan = nil
		}

		return nil
	}
	return commons.PanicSystemError
}

func (instance *Guardian) handleSchedulerEvent(schedule *lygo_scheduler.SchedulerTask) {
	if len(schedule.Arguments) > 0 {
		var m map[string]interface{}
		_ = lygo_json.Read(schedule.String(), &m)
		if action, b := schedule.Arguments[0].(*commons.Action); b {
			// run the program if not already running
			// same task program cannot run in multiple instances.
			// just one instance at a time
			instance.taskManager.RunSingle(schedule.Uid, m, action)
		}
	}
}

func (instance *Guardian) handleSchedulerError(strErr string) {
	if nil != instance {
		err := lygo_errors.Prefix(errors.New(strErr), "SchedulerError:").Error()
		if nil != instance.logger {
			instance.logger.Error(err)
		}
		instance.events.EmitAsync(commons.EventOnError, err)
	}
}

func (instance *Guardian) handleDoStopEvent(_ *lygo_events.Event) {
	if nil != instance {
		_ = instance.Stop()
	}
}

//----------------------------------------------------------------------------------------------------------------------
//	S T A T I C
//----------------------------------------------------------------------------------------------------------------------

func loadModule(path string) ([]byte, error) {
	path = lygo_paths.Concat(lygo_paths.WorkspacePath("modules"), path)
	return lygo_io.ReadBytesFromFile(path)
}
