package updater

import (
	"bitbucket.org/lygo/lygo_app_guardian/app/commons"
	"bitbucket.org/lygo/lygo_commons/lygo_conv"
	"bitbucket.org/lygo/lygo_commons/lygo_fmt"
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_commons/lygo_rnd"
	lygo_updater "bitbucket.org/lygo/lygo_updater/src"
	"os"
	"path/filepath"
	"sync"
	"time"
)

/**
Script Update Manager
- check and update scripts

*/

// ---------------------------------------------------------------------------------------------------------------------
//		t y p e
// ---------------------------------------------------------------------------------------------------------------------

type UpdateManager struct {
	dirWork           string // guardian workspace
	dirUpdates        string // where updater objects are stored
	dirPrograms       string
	dirVariables      map[string]string
	logger            *commons.Logger
	appSettings       *commons.GuardianSettings
	settings          *commons.GuardianUpdaterSettings
	methodStopOneTask func(uid string)
	methodStopAllTask func()
	updaters          map[string]*lygo_updater.Updater
	fileMux           sync.Mutex
	mapMux            sync.Mutex
}

// ---------------------------------------------------------------------------------------------------------------------
//		c o n s t r u c t o r
// ---------------------------------------------------------------------------------------------------------------------

func NewUpdateManager(mode, dirWork string, variables map[string]string, logger *commons.Logger, settings *commons.GuardianSettings, methodStopOneTask func(uid string), methodStopAllTask func()) *UpdateManager {

	// create instance
	instance := new(UpdateManager)
	instance.dirWork = dirWork
	instance.dirUpdates = lygo_paths.Concat(instance.dirWork, "updates") // updates root. usually ./_guardian/updates
	instance.dirPrograms = lygo_paths.Concat(instance.dirWork, "programs")
	instance.dirVariables = variables
	instance.logger = logger
	instance.appSettings = settings
	instance.methodStopOneTask = methodStopOneTask
	instance.methodStopAllTask = methodStopAllTask
	instance.updaters = make(map[string]*lygo_updater.Updater)

	_ = lygo_paths.Mkdir(instance.dirUpdates + "/")

	s, e := loadSettings(mode)
	if nil == e && nil != s {
		instance.settings = s
	}

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//		p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *UpdateManager) Start() {
	if nil != instance {
		instance.start()
	}
}

func (instance *UpdateManager) Stop() {
	if nil != instance {
		instance.stop()
	}
}

func (instance *UpdateManager) AddTask(uid, dirProgram string, settings *lygo_updater.Settings) {
	if nil != instance && nil != settings {
		instance.newUpdater(uid, dirProgram, settings)
	}
}

// ---------------------------------------------------------------------------------------------------------------------
//		p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *UpdateManager) start() {
	if nil != instance.settings && instance.settings.Enable {
		// start updater for each action
		for _, updater := range instance.settings.Updaters {
			if nil != updater.PackageFiles {
				instance.newUpdater(updater.Uid, "", updater)
			} else {
				instance.writeDisabled(updater.Uid)
			}
		}
	}
}

func (instance *UpdateManager) stop() {
	instance.mapMux.Lock()
	defer instance.mapMux.Unlock()
	for _, updater := range instance.updaters {
		updater.Stop()
	}
	instance.updaters = make(map[string]*lygo_updater.Updater)
}

func (instance *UpdateManager) newUpdater(uid, dirProgram string, settings *lygo_updater.Settings) {
	instance.mapMux.Lock()
	defer instance.mapMux.Unlock()

	if len(uid) == 0 {
		uid = lygo_rnd.Uuid()
	}

	// new updater
	root := lygo_paths.Concat(instance.dirUpdates, uid)
	_ = lygo_paths.Mkdir(root + string(os.PathSeparator))
	updater := lygo_updater.NewUpdater(lygo_json.Stringify(settings))
	updater.SetUid(uid)
	updater.SetRoot(lygo_paths.Concat(instance.dirUpdates, uid))
	updater.OnEvent(instance.onUpdaterEvent)
	// add some path variables to updater
	for k, v := range instance.dirVariables {
		updater.SetVariable(k, v)
	}
	if len(dirProgram) > 0 {
		updater.SetVariable(commons.PathDirProgram, filepath.Dir(lygo_paths.WorkspacePath(dirProgram)))
	} else {
		updater.SetVariable(commons.PathDirProgram, instance.dirWork) // set to workspace if program dir is missing
	}

	_, _, _, _, err := updater.Start()
	if nil == err {
		instance.updaters[uid] = updater
	} else {
		// error launching updater
		updater.Stop()
		instance.writeError(uid, err.Error())
	}
}

func (instance *UpdateManager) onUpdaterEvent(updater *lygo_updater.Updater, name string, args []interface{}) {
	if nil != instance && nil != updater {
		uid := updater.GetUid()
		switch name {
		case commons.EventOnError:
			if len(args) > 0 {
				err := lygo_conv.ToString(args[0])
				instance.writeError(uid, err)
			}
		case commons.EventOnUpgrade:
			if len(args) > 1 {
				from := lygo_conv.ToString(args[0])
				to := lygo_conv.ToString(args[1])
				files := lygo_conv.ToArrayOfString(args[2])
				instance.writeUpgrade(uid, from, to, files)
				if nil != instance.methodStopOneTask {
					// stop running task after an update
					instance.methodStopOneTask(uid)
				}
			}
		case commons.EventOnStart:
			if len(args) > 0 {
				command := lygo_conv.ToString(args[0])
				instance.writeStart(uid, command)
			}
		case commons.EventOnQuit:
			if len(args) > 1 {
				command := lygo_conv.ToString(args[0])
				pid := lygo_conv.ToInt(args[1])
				instance.writeStop(uid, command, pid)
			}
		default:
			// non handled by default
		}
	}
}

func (instance *UpdateManager) writeStart(uid string, command string) {
	if nil != instance {
		data := map[string]interface{}{
			"last-event": "start",
			"start": map[string]interface{}{
				"timestamp": time.Now().Unix(),
				"time":      lygo_fmt.FormatDate(time.Now(), "yyyyMMdd HH:mm:ss"),
				"command":   command,
			},
		}
		instance.write(lygo_paths.Concat(instance.dirUpdates, uid+".json"), data)
	}
}

func (instance *UpdateManager) writeStop(uid string, command string, pid int) {
	if nil != instance {
		data := map[string]interface{}{
			"last-event": "stop",
			"stop": map[string]interface{}{
				"timestamp": time.Now().Unix(),
				"time":      lygo_fmt.FormatDate(time.Now(), "yyyyMMdd HH:mm:ss"),
				"command":   command,
				"pid":       pid,
			},
		}
		instance.write(lygo_paths.Concat(instance.dirUpdates, uid+".json"), data)
	}
}

func (instance *UpdateManager) writeUpgrade(uid string, version string, version2 string, files []string) {
	if nil != instance {
		data := map[string]interface{}{
			"last-event": "upgrade",
			"upgrade": map[string]interface{}{
				"timestamp": time.Now().Unix(),
				"time":      lygo_fmt.FormatDate(time.Now(), "yyyyMMdd HH:mm:ss"),
				"from":      version,
				"to":        version2,
				"files":     lygo_conv.ToString(files),
			},
		}
		instance.write(lygo_paths.Concat(instance.dirUpdates, uid+".json"), data)
	}
}

func (instance *UpdateManager) writeError(uid, error string) {
	data := map[string]interface{}{
		"last-event": "error",
		"error": map[string]interface{}{
			"timestamp": time.Now().Unix(),
			"time":      lygo_fmt.FormatDate(time.Now(), "yyyyMMdd HH:mm:ss"),
			"message":   error,
		},
	}
	instance.write(lygo_paths.Concat(instance.dirUpdates, uid+".json"), data)
}

func (instance *UpdateManager) writeDisabled(uid string) {
	data := map[string]interface{}{
		"last-event": "disabled",
		"disabled": map[string]interface{}{
			"timestamp": time.Now().Unix(),
			"time":      lygo_fmt.FormatDate(time.Now(), "yyyyMMdd HH:mm:ss"),
			"disabled":  true,
		},
	}
	instance.write(lygo_paths.Concat(instance.dirUpdates, uid+".json"), data)
}

func (instance *UpdateManager) write(filename string, data map[string]interface{}) {
	instance.fileMux.Lock()
	defer instance.fileMux.Unlock()

	// get existing
	dataToWrite := map[string]interface{}{}
	if b, _ := lygo_paths.Exists(filename); b {
		// merge existing
		bytes, err := lygo_io.ReadBytesFromFile(filename)
		if nil == err {
			if existingData, b := lygo_json.BytesToMap(bytes); b {
				for k, v := range existingData {
					dataToWrite[k] = v
				}
			}
		}
	}

	// overwrite with data
	for k, v := range data {
		dataToWrite[k] = v
	}

	_, _ = lygo_io.WriteBytesToFile(lygo_json.Bytes(dataToWrite), filename)
}

// ---------------------------------------------------------------------------------------------------------------------
//		S T A T I C
// ---------------------------------------------------------------------------------------------------------------------

func loadSettings(mode string) (*commons.GuardianUpdaterSettings, error) {
	path := lygo_paths.WorkspacePath("updater." + mode + ".json")
	settings := new(commons.GuardianUpdaterSettings)
	text, err := lygo_io.ReadTextFromFile(path)
	if nil != err {
		return settings, err
	}
	err = lygo_json.Read(text, &settings)
	return settings, err
}
