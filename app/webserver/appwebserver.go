package webserver

import (
	"bitbucket.org/lygo/lygo_app_guardian/app/commons"
	"bitbucket.org/lygo/lygo_app_guardian/app/http"
	"bitbucket.org/lygo/lygo_app_guardian/app/secure"
	"bitbucket.org/lygo/lygo_app_guardian/app/tasks"
	"bitbucket.org/lygo/lygo_app_guardian/app/webserver_authentication"
	"fmt"
	"github.com/gofiber/fiber/v2"
)

// ---------------------------------------------------------------------------------------------------------------------
//		t y p e
// ---------------------------------------------------------------------------------------------------------------------

type AppWebserver struct {
	mode          string
	dirWork       string // guardian workspace
	logger        *commons.Logger
	secureManager *secure.AppSecure
	taskManager   *tasks.TaskManager

	webserver               *http.Webserver
	webserverAuthentication *webserver_authentication.AppWebserverAuthentication
}

func NewAppWebserver(mode string, dirWork string, l *commons.Logger, s *secure.AppSecure, t *tasks.TaskManager) *AppWebserver {
	instance := new(AppWebserver)
	instance.mode = mode
	instance.dirWork = dirWork
	instance.logger = l
	instance.secureManager = s
	instance.taskManager = t
	instance.webserver = http.NewWebserver("webserver", mode, dirWork)

	_ = instance.initHandlers()

	_ = instance.initAuthenticationHandlers()

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//		p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *AppWebserver) Start() {
	if nil != instance && nil != instance.webserver {
		if instance.webserver.Start() {
			// run initialization scripts
			instance.runInitializationScripts()
		}
	}
}

func (instance *AppWebserver) Stop() {
	if nil != instance && nil != instance.webserver {
		instance.webserver.Stop()
		if nil != instance.webserverAuthentication {
			instance.webserverAuthentication.Stop()
		}
	}
}

func (instance *AppWebserver) LocalUrl() string {
	if nil != instance && nil != instance.webserver {
		return instance.webserver.LocalUrl()
	}
	return ""
}

// ---------------------------------------------------------------------------------------------------------------------
//		p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *AppWebserver) initHandlers() error {
	if nil != instance && nil != instance.webserver && instance.webserver.IsEnabled() && nil != instance.webserver.Settings() {
		handlers := make([]http.IRouteHandler, 0)
		routing := instance.webserver.Settings().Routing
		for _, route := range routing {
			handler := NewRouteHandler(route, instance.runScript)
			handlers = append(handlers, handler)
		}
		return instance.webserver.Initialize(handlers)
	}
	return nil
}

func (instance *AppWebserver) initAuthenticationHandlers() error {
	if nil != instance && nil != instance.webserver && instance.webserver.IsEnabled() && nil != instance.webserver.Settings() {
		authenticationSettings := instance.webserver.Settings().Authentication
		if nil != authenticationSettings {
			instance.webserverAuthentication = webserver_authentication.NewAppWebserverAuthentication(instance.mode,
				instance.dirWork, instance.webserver.HttpRoot(), instance.logger, authenticationSettings, instance.webserver, instance.secureManager)
			// initialize templates and authentication engine
			files, errs := instance.webserverAuthentication.Start()
			if len(errs) > 0 {
				instance.logger.Warn("Authentication Templates are not enabled: ", errs)
			} else {
				if len(files) > 0 {
					instance.logger.Debug(fmt.Sprintf("Authentication Templates downloaded %v new files.", len(files)))
				} else {
					instance.logger.Debug("Authentication Templates are enabled.")
				}
			}
		}
	}
	return nil
}

func (instance *AppWebserver) runInitializationScripts() {
	if nil != instance && nil != instance.webserver {
		scripts := instance.webserver.Settings().Initialize
		for _, script := range scripts {
			if len(script.Program) > 0 {
				_ = instance.runScript(nil, script, "")
			}
		}
	}
}

func (instance *AppWebserver) runScript(ctx *fiber.Ctx, action *commons.Action, functionName string, args ...interface{}) error {
	variables := instance.taskManager.Variables().Extend(action.Variables)
	if http.AuthenticateRequest(ctx, instance.secureManager.Auth0(), variables, true) {
		response := instance.taskManager.Run(action.Uid, action, functionName, args...)
		if nil != response {
			if nil != ctx {
				// something returned from script
				switch action.GetRuntimeName() {
				case "js":
					if !response.ExecutedFunction {
						body := string(ctx.Response().Body())
						status := ctx.Response().StatusCode()
						if len(body) == 0 && status != 200 {
							return http.WriteResponse(ctx, response.Value, response.Err)
						} else {
							// already written into response
							// fmt.Println(body)
						}
					}
				case "aql":
					return http.WriteResponse(ctx, response.Value, response.Err)
				case "sql":
					return http.WriteResponse(ctx, response.Value, response.Err)
				case "py":
					return http.WriteResponse(ctx, response.Value, response.Err)
				}
			}
			return response.Err
		}
	}
	return nil
}
