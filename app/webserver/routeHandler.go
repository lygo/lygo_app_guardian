package webserver

import (
	"bitbucket.org/lygo/lygo_app_guardian/app/commons"
	"bitbucket.org/lygo/lygo_app_guardian/app/http"
	"github.com/gofiber/fiber/v2"
	"strings"
)

// ---------------------------------------------------------------------------------------------------------------------
//		t y p e
// ---------------------------------------------------------------------------------------------------------------------

type ExecutorCallback func(ctx *fiber.Ctx, action *commons.Action, functionName string, args ...interface{}) error

type RouteHandler struct {
	method   string
	endpoint string
	action   *commons.Action

	runScript ExecutorCallback
}

func NewRouteHandler(config *http.WebServerSettingsRouting, runScript ExecutorCallback) *RouteHandler {
	instance := new(RouteHandler)
	instance.method = strings.ToLower(config.Method)
	instance.endpoint = config.Endpoint
	instance.action = config.Action
	instance.runScript = runScript

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//		p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *RouteHandler) Method() string {
	if nil != instance {
		return instance.method
	}
	return ""
}

func (instance *RouteHandler) Endpoint() string {
	if nil != instance {
		return instance.endpoint
	}
	return ""
}

func (instance *RouteHandler) Handle(ctx *fiber.Ctx) error {
	if nil != instance {
		err := instance.run(ctx)
		if instance.isMiddleware() {
			return ctx.Next()
		}
		return err
	}

	return nil
}

// ---------------------------------------------------------------------------------------------------------------------
//		p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *RouteHandler) isReady() bool {
	if nil != instance && nil != instance.action {
		return len(instance.action.Program) > 0
	}
	return false
}

func (instance *RouteHandler) isMiddleware() bool {
	if nil != instance {
		return instance.method == "middleware"
	}
	return false
}

func (instance *RouteHandler) run(ctx *fiber.Ctx) error {
	if nil != instance && instance.isReady() && nil != instance.runScript {
		return instance.runScript(ctx, instance.action, "handle", ctx)
	}
	return commons.EndpointNotReadyError
}
