package main

import (
	"bitbucket.org/lygo/lygo_app_guardian/app"
	"bitbucket.org/lygo/lygo_app_guardian/app/commons"
	"bitbucket.org/lygo/lygo_commons/lygo_conv"
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_commons/lygo_strings"
	"flag"
	"fmt"
	"os"
	"path"
	"time"
)

var logger *commons.Logger

//----------------------------------------------------------------------------------------------------------------------
//	l a u n c h e r
//----------------------------------------------------------------------------------------------------------------------

func main() {
	// PANIC RECOVERY
	defer func() {
		if r := recover(); r != nil {
			// recovered from panic
			message := lygo_strings.Format("[panic] APPLICATION %s ERROR: %s", commons.Name, r)
			if nil != logger {
				logger.Error(message)
				time.Sleep(3 * time.Second) // wait logger can write
			} else {
				fmt.Println(message)
			}
		}
	}()

	// fmt.Println("GUARDIAN STARTING")

	//-- command flags --//
	// run
	cmdRun := flag.NewFlagSet("run", flag.ExitOnError)
	dirStart := cmdRun.String("dir_start", lygo_paths.Absolute("./"), "Set a particular folder as start directory")
	dirApp := cmdRun.String("dir_app", lygo_paths.Absolute("./"), "Set a particular folder as binary directory")
	dirWork := cmdRun.String("dir_work", lygo_paths.Absolute("./_guardian"), "Set a particular folder as main workspace")
	mode := cmdRun.String("m", commons.ModeProduction, "Mode allowed: 'debug' or 'production'")
	quit := cmdRun.String("s", "", "Quit Command: Write a command (ex: 'stop') to enable stop mode")

	// parse
	if len(os.Args) > 1 {
		cmd := os.Args[1]
		switch cmd {
		case "run":
			_ = cmdRun.Parse(os.Args[2:])
		}
	}

	// fmt.Println("GUARDIAN mode:", *mode)

	// init
	initialize(dirStart, dirApp, dirWork, mode)
	lygo_paths.GetWorkspace(commons.WpDirStart).SetPath(*dirStart)
	lygo_paths.GetWorkspace(commons.WpDirApp).SetPath(*dirApp)
	lygo_paths.GetWorkspace(commons.WpDirWork).SetPath(*dirWork)

	settings, err := commons.NewGuardianSettings(*mode)
	if nil != err {
		panic(err)
	}

	if len(*quit) > 0 {
		settings.StopCmd = *quit
	}

	guardian, err := app.NewGuardian(*mode, settings)
	if nil == err {
		// get logger from guardian
		logger = guardian.GetLogger()
		logger.Info(lygo_strings.Format("APPLICATION %s v.%s mode '%s'", commons.Name, commons.Version, *mode))
		logger.Info(lygo_strings.Format("DIR_START: %s", lygo_paths.GetWorkspace(commons.WpDirStart).GetPath()))
		logger.Info(lygo_strings.Format("DIR_APP: %s", lygo_paths.GetWorkspace(commons.WpDirApp).GetPath()))
		logger.Info(lygo_strings.Format("DIR_WORK: %s", lygo_paths.GetWorkspace(commons.WpDirWork).GetPath()))

		err = guardian.Start()
		if nil != err {
			panic(err)
		}
		guardian.Wait() // lock waiting guardian exit or close gracefully
	} else {
		fmt.Println(fmt.Sprintf("Error %v", err))
	}
	fmt.Println(fmt.Sprintf("Quit %s", commons.Name))
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func initialize(dirStart, dirApp, dirWork *string, mode *string) {
	file := lygo_paths.Concat(path.Dir(*dirWork), "init.json")
	if b, _ := lygo_paths.Exists(file); b {
		data, err := lygo_json.ReadMapFromFile(file)

		if nil == err {

			// WORKSPACE
			if len(*dirWork) == 0 {
				w := lygo_conv.ToString(data["workspace"])
				if len(w) > 0 {
					*dirWork = w
				}
			}

			// MODE
			m := lygo_conv.ToString(data["mode"])
			if len(m) == 0 {
				m = commons.ModeProduction
			}
			*mode = m
		}
	}
}
