##!/bin/sh

BASE="1.1"
# Get latest build
BUILD=$(<build_version.txt)
chrlen=${#BUILD}
if [ $chrlen = 0 ]
then
  BUILD=0
fi

echo "START BUILDING LINUX VERSION $BASE.$BUILD..."

## linux
env GOOS=linux GOARCH=386 go build -o ./_build/linux/guardian main.go


# downloads
## RUNTIME
#rm ./_playground/repo_updater/_guardian/www/downloads/runtime/linux/guardian
#cp ./_build/linux/guardian ./_playground/repo_updater/_guardian/www/downloads/runtime/linux/
#echo "COPY VERSION $BASE.$BUILD TO ./_playground/repo_updater/_guardian/www/downloads/runtime/linux/"