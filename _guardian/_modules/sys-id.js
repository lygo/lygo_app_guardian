var sys = require("sys");
var path = require("path");
var fs = require("fs");

// -------------------------------------------------------------------------
// p u b l i c
// -------------------------------------------------------------------------

/**
 * Returns unique machine ID.
 * @param opt_filename Optional file name. Default is: "serial.txt"
 * @returns {string|undefined}
 */
function get(opt_filename) {
    try {
        opt_filename = opt_filename||"serial.txt";
        var id = sys.id();
        var filename = path.resolve(__dirname, opt_filename);

        if (fs.existsSync(filename)) {
            // read the file
            var txt = fs.readFileSync(filename);
            if (!!txt) {
                id = txt.trim();
            } else {
                // creates the file
                fs.writeFileSync(filename, id);
            }
        } else {
            // creates the file
            fs.writeFileSync(filename, id);
        }


        return id; // send id
    } catch (err) {
        console.error("sys-id.js", err);
    }
    return undefined; //  nothing back
}

// -------------------------------------------------------------------------
// p r i v a t e
// -------------------------------------------------------------------------


// -------------------------------------------------------------------------
// e x p o r t s
// -------------------------------------------------------------------------

module.exports = {
    get: get
}