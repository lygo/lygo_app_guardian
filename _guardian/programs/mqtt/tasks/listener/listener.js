var mq = use("message-queue");

try {
    console.log("MQTT listener STARTED")

    // connect to broker
    const conn = mq.newConnection({
        "protocol": "amqp",
        "secret": "", // optional. If assigned messages are encrypted
        "url": "amqp://guest:guest@localhost:5672" 
    });

    // test broker connection
    if (!!conn && conn.ping()) {

        // consume messages
        try {
            var consumer = conn.newListener({
                "queue": "prod_logs_consumer1_queue",
                "consumer-tag": "",
                "no-local": false,
                "auto-ack": false,
                "exclusive": false,
                "no-wait": false,
                "args": null
            });
            consumer.listen(function (message) {
                console.log(JSON.stringify(message))
                console.log(JSON.stringify(this))
            });
        } catch (err) {
            console.error("Error creating new consumer:", err);
        }
    } else {
        console.error("Connection not available. This should not happen immediatelly after a connection creation.");
    }
} catch (err) {
    console.error("Broker not available:", err);
}