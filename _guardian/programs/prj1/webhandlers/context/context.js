try{
    var global = _global;

    // add number
    var i = !!global.Get("num")?parseInt(global.Get("num")):0||0;
    i++;
    global.Set("num", i);
    console.log("CONTEXT", "Global num: " + i);
}catch(err){
    console.error("CONTEXT", err);
}