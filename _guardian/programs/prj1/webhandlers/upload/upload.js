try{
    console.log("UPLOAD TEST");
    //console.log("runtime.context", JSON.stringify(runtime.context));
    var req = runtime.context.req;
    var files = req.multipart().files;
    var file = files[0];

    var path = file.save("./uploads/");

    runtime.context.res.json({
        file: path
    });
}catch(e){
    console.error("UPLOAD", e);
    runtime.context.res.json({
        error: e.message
    });
}