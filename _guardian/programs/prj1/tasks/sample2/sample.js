try{
    console.log("SAMPLE 2");
    var database = runtime.context["commons"]["database"];
    console.log("SAMPLE 2", JSON.stringify(database));

    var global = _global;
    // add number
    var i = !!global.Get("num")?parseInt(global.Get("num")):0||0;
    i++;
    global.Set("num", i);
    console.log("SAMPLE 2", "Global num: " + i);

}catch(e){
    console.error("SAMPLE 2", e);
}
