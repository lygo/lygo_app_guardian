try{
    console.log("SAMPLE 1");
    // console.log("runtime.context", JSON.stringify(runtime.context));
    // var database = runtime.context["commons"]["database"];
    // console.log(JSON.stringify(database));

    var global = _global;

    // add number
    var i = !!global.Get("num")?parseInt(global.Get("num")):0||0;
    i++;
    global.Set("num", i);
    console.log("SAMPLE 1", "Global num: " + i);

    // add pointer to function
    var f = global.Get("func");
    if (!f){
        f = function (i){
            console.log("SAMPLE 1", "Global num from function: " + i);
        }
        global.Set("func", f);
    }
    f = global.Get("func");
    f(i);

    // add pointer to object
    var o = global.Get("obj1");
    if (!o){
        o = new Obj1();
        global.Set("obj1", o);
    }
    var o1 = global.Get("obj1");
    o1.Inner.count(new Date().getTime());
}catch(e){
    console.error("SAMPLE 1", e);
}

function ObjInner(){
    this.count = function (timestamp){
        console.log("SAMPLE 1", timestamp, "Global num from Obj1.Inner: " + parseInt(global.Get("num")));
    }
}

function Obj1(){
    this.Inner = new ObjInner();
    this.count = function (timestamp){
        console.log("SAMPLE 1", timestamp, "Global num from Obj1: " + parseInt(global.Get("num")));
    }
}