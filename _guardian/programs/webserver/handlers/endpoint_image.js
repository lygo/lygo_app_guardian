console.log("endpoint_image LISTENING");

(function () {

    var path = use("path");
    var fu = use("file-utils");

    function handle(req, res) {
        try {
            var root = path.resolve("_guardian/programs/webserver/resources");
            fu.fileWrite(path.join(root, "context.json"), JSON.stringify(window.context));

            var imgName = req.params["img_name"];
            var imgPath = path.join(root, imgName);

            console.log("IMAGE: ", imgPath);

            var bytes = fu.fileReadBytes(imgPath);
            console.log("BYTES:", bytes.length);

            if (bytes.length>0){
                res.send(bytes, "image/png");
            } else {
                res.html("<b>HELLO</b> WORLD! " + new Date() + "<br>");
            }
        } catch (err) {
            res.html("<b>ERROR</b>" + new Date() +
                "<br>" + JSON.stringify(req) +
                "<br>" + "ERROR: " + err);
            console.error(err);
        }
    }

    // ---------------------------------------------------------
    //  e x p o r t
    // ---------------------------------------------------------

    return {
        handle: handle
    };

})();