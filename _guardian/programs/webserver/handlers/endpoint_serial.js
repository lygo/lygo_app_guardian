(function (){
    console.log("req", _req)
    console.log("res", _res)
    var id = use("sys-id.min.js");

    function handle(req, res) {
        res.text(id.get());
    }

    // ---------------------------------------------------------
    //  e x p o r t
    // ---------------------------------------------------------

    return { handle: handle };
    // return { message: "HELLO JAVASCRIPT ENDPOINT" };
})();