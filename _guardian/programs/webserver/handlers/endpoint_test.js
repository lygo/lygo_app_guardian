console.log("endpoint_test LISTENING");

(function () {


    function handle(req, res) {
        try {
            var auth0 = _auth0;
            resp = auth0.tokenValidate("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiMjEzODBjMTc5ZGViM2JlMTQzZmNhNzIyYTMwYTExZGQiLCJwYXlsb2FkIjp7ImNvbmZpcm1lZCI6dHJ1ZSwicmVmcmVzaF91dWlkIjoiNzA4M2MxYjNjNWRhNWY1MzRjMjI4NmMwODUxM2JmNTciLCJ1c2VyX25hbWUiOiJkYXZpZGUuYWllbGxvQGJvdGlrYS5haSIsInVzZXJfcHN3IjoiNjYzOGJmNzg2ZGMyYmNkNTVlMWUwOGQ0NWQyYzY1Y2UifSwic2VjcmV0X3R5cGUiOiJhY2Nlc3MiLCJleHAiOjE2MDgxMDg5MzMsImp0aSI6IjcwODNjMWIzYzVkYTVmNTM0YzIyODZjMDg1MTNiZjU3In0.3ZXQWEVBccZo--_K8dacMT6RKImYOwXL2JVAeCZQ_J0");

            var driver = _database;
            var driver_name = !!driver.name?driver.name():driver; // driver name or error

            res.html("<b>HELLO</b> WORLD! " + new Date() +
                "<br>" + JSON.stringify(req) +
                "<br>" + "DB: " + driver_name);

        } catch (err) {
            res.html("<b>ERROR</b>" + new Date() +
                "<br>" + JSON.stringify(req) +
                "<br>" + "ERROR: " + err);
            console.error(err);
        }
    }

    // ---------------------------------------------------------
    //  e x p o r t
    // ---------------------------------------------------------

    return {
        handle: handle
    };

})();