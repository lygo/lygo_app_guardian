try{
    console.log("INIT 2");
    console.log("runtime.context", JSON.stringify(runtime.context));

    var global = _global;
    // add number
    var i = !!global.Get("num")?parseInt(global.Get("num")):0||0;
    global.Set("num", i);
    console.log("init2.js", "Global num is: " + i);
}catch(err){
    console.error("init2.js", err);
}
