var nio = require("nio");
var files = require("file-utils");
var workspace = require("workspace");

try {
    var serverReady = _global.Get("server")||false;
    if (!!serverReady){
        var client = nio.newClient("127.0.0.1:10010");
        client.secure(false);
        client.open();

        var file = workspace.resolve("data.csv");

        var resp = client.send("file_upload", "data.csv", files.fileReadBytes(file));
        console.log("Server Response:", resp);

        client.close();
    }
} catch (err) {
    console.error(err);
}



