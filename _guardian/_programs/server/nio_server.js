var nio = require("nio");
var crypto = require("crypto-utils");
var workspace = require("workspace");
var sysId = require("sys-id.js");

var VERSION = "1.1.1";

function onMessage(eventName, params) {
    console.log(VERSION, eventName, "params count:" + params.length);
    if (eventName === "file_upload" && params.length === 2) {
        var file = params[0];
        var text = crypto.decodeBase64ToText(params[1]);
        console.log(VERSION, "file: ", file, "\tdata: ", !!text ? text.length : 0);
    }
}

var ID = sysId.get();

// log starting
console.log("STARTING...", VERSION, ID);
// log internal variables
console.log("DIR_START = ", __dir_start);
console.log("DIR_APP = ", __dir_app);
console.log("DIR_WORK = ", __dir_work);
console.log("DIR_NAME = ", __dirname);
console.log("FILE_NAME = ", __filename);
// log workspace methods
console.log("workspace.resolve() = ", workspace.resolve());
console.log("workspace.resolve('./') = ", workspace.resolve('./'));
console.log("workspace.resolve('./', '/app', '/dir') = ", workspace.resolve('./', '/app', '/dir'));


var server = nio.newServer(10010);
try {
    server.open();
    server.listen("*", onMessage);

    // mark server is ready
    _global.Set("server", true);

    // lock and wait until close
    // server.join();
} catch (err) {
    server.close();
    console.error("ERROR!!:", err, VERSION);
}
