(function () {
    try {
        var ctx = _context;
        if (!ctx.Get("count")) {
            ctx.Set("count", 0);
        }
        ctx.Set("count", ctx.Get("count")+1);

        var global = _global;
        if (!global.Get("count")) {
            global.Set("count", 0);
        }
        global.Set("count", global.Get("count")+1);

        console.info("counter_2", ctx.Get("count"), global.Get("count"));
    } catch (err) {
        console.error("counter_2", err);
    }
})();