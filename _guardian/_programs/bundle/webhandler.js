/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/webhandler/launcher.ts");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/commons.ts":
/*!************************!*\
  !*** ./src/commons.ts ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
var w = window;
/* harmony default export */ __webpack_exports__["default"] = ({
    context: w.context,
});


/***/ }),

/***/ "./src/webhandler/launcher.ts":
/*!************************************!*\
  !*** ./src/webhandler/launcher.ts ***!
  \************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _commons__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../commons */ "./src/commons.ts");

var Handler = /** @class */ (function () {
    function Handler() {
    }
    Handler.prototype.handle = function (req, res) {
        console.log("endpoint_test LISTENING");
        try {
            var auth0 = _commons__WEBPACK_IMPORTED_MODULE_0__["default"].context["auth0"];
            var resp = auth0.tokenValidate("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiMjEzODBjMTc5ZGViM2JlMTQzZmNhNzIyYTMwYTExZGQiLCJwYXlsb2FkIjp7ImNvbmZpcm1lZCI6dHJ1ZSwicmVmcmVzaF91dWlkIjoiNzA4M2MxYjNjNWRhNWY1MzRjMjI4NmMwODUxM2JmNTciLCJ1c2VyX25hbWUiOiJkYXZpZGUuYWllbGxvQGJvdGlrYS5haSIsInVzZXJfcHN3IjoiNjYzOGJmNzg2ZGMyYmNkNTVlMWUwOGQ0NWQyYzY1Y2UifSwic2VjcmV0X3R5cGUiOiJhY2Nlc3MiLCJleHAiOjE2MDgxMDg5MzMsImp0aSI6IjcwODNjMWIzYzVkYTVmNTM0YzIyODZjMDg1MTNiZjU3In0.3ZXQWEVBccZo--_K8dacMT6RKImYOwXL2JVAeCZQ_J0");
            var driver = _commons__WEBPACK_IMPORTED_MODULE_0__["default"].context["database"];
            var driver_name = !!driver.name ? driver.name() : driver; // driver name or error
            res.html("<b>HELLO</b> WORLD! " + new Date() +
                "<br>" + JSON.stringify(req) +
                "<br>" + "DB: " + driver_name);
        }
        catch (err) {
            res.html("<b>ERROR</b>" + new Date() +
                "<br>" + JSON.stringify(req) +
                "<br>" + "ERROR: " + err);
            console.error(err);
        }
    };
    return Handler;
}());
var handler = new Handler();
_commons__WEBPACK_IMPORTED_MODULE_0__["default"].context["response"] = {
    handle: handler.handle,
};


/***/ })

/******/ });
//# sourceMappingURL=webhandler.js.map