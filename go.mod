module bitbucket.org/lygo/lygo_app_guardian

go 1.16

require (
	bitbucket.org/lygo/lygo_commons v0.1.113
	bitbucket.org/lygo/lygo_email v0.1.5
	bitbucket.org/lygo/lygo_events v0.1.9
	bitbucket.org/lygo/lygo_ext_auth0 v0.1.19
	bitbucket.org/lygo/lygo_ext_dbal v0.1.30
	bitbucket.org/lygo/lygo_ext_http v0.1.19
	bitbucket.org/lygo/lygo_ext_logs v0.1.6
	bitbucket.org/lygo/lygo_ext_scripting v0.1.103
	bitbucket.org/lygo/lygo_ext_sms v0.1.5
	bitbucket.org/lygo/lygo_nio v0.1.4 // indirect
	bitbucket.org/lygo/lygo_scheduler v0.1.12
	bitbucket.org/lygo/lygo_updater v0.1.29
	github.com/Djarvur/go-err113 v0.1.0 // indirect
	github.com/alexkohler/prealloc v1.0.0 // indirect
	github.com/cbroglie/mustache v1.2.2
	github.com/daixiang0/gci v0.2.9 // indirect
	github.com/dop251/goja v0.0.0-20210722124603-c2068308df1e
	github.com/esimonov/ifshort v1.0.2 // indirect
	github.com/fsnotify/fsnotify v1.4.9 // indirect
	github.com/go-critic/go-critic v0.5.7 // indirect
	github.com/gofiber/fiber/v2 v2.15.0
	github.com/gofiber/websocket/v2 v2.0.7 // indirect
	github.com/gofrs/flock v0.8.1 // indirect
	github.com/golangci/golangci-lint v1.41.1 // indirect
	github.com/golangci/misspell v0.3.5 // indirect
	github.com/golangci/revgrep v0.0.0-20210208091834-cd28932614b5 // indirect
	github.com/google/go-cmp v0.5.6 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/gordonklaus/ineffassign v0.0.0-20210522101830-0589229737b2 // indirect
	github.com/gostaticanalysis/analysisutil v0.7.1 // indirect
	github.com/jirfag/go-printf-func-name v0.0.0-20200119135958-7558a9eaa5af // indirect
	github.com/kulti/thelper v0.4.0 // indirect
	github.com/kyoh86/exportloopref v0.1.8 // indirect
	github.com/ldez/gomoddirectives v0.2.2 // indirect
	github.com/makiuchi-d/gozxing v0.0.1 // indirect
	github.com/matoous/godox v0.0.0-20210227103229-6504466cf951 // indirect
	github.com/mattn/go-colorable v0.1.8 // indirect
	github.com/mattn/go-isatty v0.0.13 // indirect
	github.com/mattn/go-runewidth v0.0.13 // indirect
	github.com/mgechev/revive v1.0.9 // indirect
	github.com/nbutton23/zxcvbn-go v0.0.0-20210217022336-fa2cb2858354 // indirect
	github.com/nishanths/exhaustive v0.2.3 // indirect
	github.com/olekukonko/tablewriter v0.0.5 // indirect
	github.com/polyfloyd/go-errorlint v0.0.0-20210722154253-910bb7978349 // indirect
	github.com/prometheus/common v0.29.0 // indirect
	github.com/prometheus/procfs v0.7.1 // indirect
	github.com/quasilyte/regex/syntax v0.0.0-20200805063351-8f842688393c // indirect
	github.com/rivo/uniseg v0.2.0 // indirect
	github.com/savsgio/gotils v0.0.0-20210617111740-97865ed5a873 // indirect
	github.com/securego/gosec/v2 v2.8.1 // indirect
	github.com/sourcegraph/go-diff v0.6.1 // indirect
	github.com/spf13/cast v1.3.1 // indirect
	github.com/spf13/cobra v1.2.1 // indirect
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/stretchr/objx v0.3.0 // indirect
	github.com/tdakkota/asciicheck v0.0.0-20200416200610-e657995f937b // indirect
	github.com/tetafro/godot v1.4.8 // indirect
	github.com/timakin/bodyclose v0.0.0-20210704033933-f49887972144 // indirect
	github.com/tomarrell/wrapcheck/v2 v2.2.0 // indirect
	github.com/tommy-muehle/go-mnd v1.3.1-0.20201008215730-16041ac3fe65 // indirect
	github.com/uudashr/gocognit v1.0.5 // indirect
	github.com/valyala/fasthttp v1.28.0 // indirect
	github.com/yeqown/go-qrcode v1.5.6 // indirect
	golang.org/x/image v0.0.0-20210628002857-a66eb6448b8d // indirect
	golang.org/x/mod v0.4.2 // indirect
	golang.org/x/tools v0.1.5 // indirect
	google.golang.org/protobuf v1.27.1 // indirect
	gopkg.in/ini.v1 v1.62.0 // indirect
	mvdan.cc/gofumpt v0.1.1 // indirect
	mvdan.cc/unparam v0.0.0-20210701114405-894c3c7ee6a6 // indirect
)
