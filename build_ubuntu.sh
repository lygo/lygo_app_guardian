##!/bin/sh
CONTAINER_NAME="guardian"

# TODO: change build path
BUILD_PATH="/Users/angelogeminiani/go/src/bitbucket.org/lygo/lygo_app_guardian/_build/ubuntu"

rm ./_build/ubuntu/guardian
docker stop $CONTAINER_NAME
docker rm $CONTAINER_NAME
docker build . -f build.Dockerfile -t $CONTAINER_NAME
docker run --name $CONTAINER_NAME -v $BUILD_PATH:/app $CONTAINER_NAME