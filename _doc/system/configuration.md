# Guardian Configuration Files #

Guardian starts reading a configuration file.
 
Depending on `--mode` (or `-m`) runtime parameter, Guardian read a "production" or "debug" file.

Example running Guardian in debug mode from terminal:
```
guardian run -m=debug -dir_start=./ -dir_app=./app -dir_work=./_guardian
```
Example above launch Guardian reading `./_guardian/settings.debug.json` configuration file.

## Anatomy of a Guardian Configuration File ##

Basically configuration file are really simple and contains just few fields:
* silent: disable verbose logging
* log-level: define logging granularity (debug, info, warn, error)
* actions: array of agents to run

```json
{
  "silent": true,
  "log-level": "info",
  "actions": [
    {
      "uid": "agent_1",
      "start_at": "",
      "timeline": "second:3",
      "program": "./programs/agent.js"
    },
    {
      "uid": "agent_2",
      "start_at": "10:30",
      "timeline": "day:1",
      "program": "./programs/agent.js"
    }
  ]
}
```

This configuration file is configuring two agents from same script.

- First agent (`agent_1`) start immediately in a continuous loop every 3 seconds.
- Second agent (`agent_2`) start 10:30AM in a continuous loop every 1 day.

The most relevant field in Guardian's configuration file is `actions`.

## Actions ##

`actions` is a field containing an array of ACTION objects. 
An action contains all configuration fields for an **agent** 
(`agents` are scheduled programs launched in Guardian runtime).

Below there's a basic agent configuration:
```json
  {
      "uid": "agent_1",
      "start_at": "",
      "timeline": "second:3",
      "program": "./programs/agent.js"
  }
```

This agent (`agent_1`) start immediately in a continuous loop every 3 seconds.

### Action Fields ###

* `uid`: Unique agent name. While you can run many times the same script file, you cannot have many instances with same name. `uid` must be unique. 
* `start_at`: Optional time to start the agent. Default is empty string. If time is not specified the agent start immediately. Otherwise, agent start at specified time. Format is: `HH:mm:ss` (10:30:00)
* `timeline`: Period for agent main loop. All agents must have a main loop, even if you want run just once. Format: `unit:value` ("millisecond:500"). 
Supported units are:
    * millisecond
    * second
    * minute
    * hour
* `program`: Script path relative to [workspace](./workspace.md).

**NOTE**: `uid` is used as file name in [logging](./workspace.md).

### Action Updater Field ###

Guardian agents support live update. 

Live update is not mandatory, it's optional and usually is not enabled by default.

To enable live update you must set updater field.

Below a sample agent with a live update service enabled:

```json
    {
      "uid": "agent_1",
      "start_at": "",
      "timeline": "second:1",
      "program": "./programs/agent.js",
      
      "updater": {
        "version_file_required": true,
        "version_file": "http://update.server/scripts/myagent/version.txt",
        "package_files": [
          {
            "file": "http://update.server/scripts/myagent/agent.js",
            "target": "$dir_program"
          }
        ],
        "scheduled_updates": [
          {
            "uid": "every_5_seconds",
            "start_at": "",
            "timeline": "second:5"
          }
        ]
      }
    }
```

Updater fields:

* `version_file_required`: If `true`, updater always force download the latest version at start if `version.txt` file does not exist locally.
* `version_file`: Remote path (file system or http) to version file (usually a text file named "`version.txt`" containing a version string. ex: "`1.0.1`")
* `package_files`: Array of files to download and target destination to save those files:
    * `file`: Remote path (file system or http) of file (zip archive, text files or binary files) to download
    * `target`: Local directory to save file. Variables are allowed (look at "variables" paragraph below).
* `scheduled_updates`: Live update schedule settings:
    * `uid`: Optional update task identifier. This field is useful in logging. It's not required and can be omitted.
    * `start_at`: Optional time to start update check. Default is empty string. If time is not specified the updater start immediately. Otherwise, updater start at specified time. Format is: `HH:mm:ss` (10:30:00)
  * `timeline`: Period for updater main loop. Supported units are:
    * millisecond
    * second
    * minute
    * hour

## Updater Variables ##

Live update uses variables to better and simpler point at working directories.

`$dir_start`: Guardian starting directory

`$dir_app`: Guardian binary directory

`$dir_work`: Guardian Workspace

`$dir_modules`: Programs Directory

`$dir_programs`: Programs Directory

`$dir_program`: Program File Directory. The directory of current agent.

### Configuration File in a Few Words ###

Above configuration file is telling Guardian:
* to run an agent named `agent_1` from `./programs/agent.js` script and check every `1 second` if is possible run again (if agent is already running, Guardian wait next loop to run again)
* to enable live update service and check every `5 seconds` for a new version. The version file is located at `http://update.server/scripts/myagent/version.txt`.
* if new version exists, updated must download 1 file (`http://update.server/scripts/myagent/agent.js`) to save into `$dir_program` directory (this is the current program directory, so file is overwritten at download)

NOTE: _Agents are never launched twice if already running_. Guardian check at every loop if an instance is 
alredy running before launch it again.

### Variables Field ###

Another optional Action's field is `variables`.
 
```
{
      "uid": "agent_1",
      "start_at": "",
      "timeline": "second:1",
      "program": "./programs/agent.js",
      "variables": {
        "HOST": "192.168.1.1",
        "ANOTHER_VARIABLE": "hello variables..."
      },
      ...
}
```

Variables declared in configuration file are passed to script engine with an underscore `_` prefix.

```javascript

var host = _HOST; // get HOST variable declared in configuration file
console.log("Host is: " + host);

``` 

_____

Below is a complete configuration file that enable Live Update and contain also variables:

```json
    {
      "uid": "agent_1",
      "start_at": "",
      "timeline": "second:1",
      "program": "./programs/agent.js",
      "variables": {
        "HOST": "192.168.1.1"
      },
      "updater": {
        "version_file_required": true,
        "version_file": "http://localhost/scripts/myagent/version.txt",
        "package_files": [
          {
            "file": "http://localhost/scripts/myagent/agent.js",
            "target": "$dir_program"
          }
        ],
        "scheduled_updates": [
          {
            "uid": "every_5_seconds",
            "start_at": "",
            "timeline": "second:5"
          }
        ]
      }
    }
```


