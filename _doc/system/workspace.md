# Workspace #

Guardian's **workspace** is the starting point where Guardian read and put all its data, files and what ever.

Everything around Guardian starts from the workspace. Everything works in the workspace.

Each Guardian instance must have a dedicated workspace where write logs, get configurations or agent's scripts.

Below a figure with a sample workspace content.

![workspace image](./workspace.png)

In above sample workspace we can find:
* logging folder: where Guardian write runtime and agent's logs
* modules: standard directory for javascript modules (reusable scripts)
* programs: standard directory for javascript agents
* settings.json files: Guardian [configuration](./configuration.md) file
* tasks directory: where Guardian write all task (agent) status
* updates directory: where Guardian write all updates status and events

Sample task json file:

```json
{
  "error": "",
  "executed": false,
  "launcher-pid": 1,
  "pid": 7511,
  "settings": {
    "Uid": "web_server",
    "Arguments": [
      {
        "uid": "web_server",
        "start_at": "",
        "timeline": "minute:1",
        "_": null,
        "program": "./programs/web_server.js",
        "variables": null,
        "updater": null
      }
    ]
  },
  "time": "20200920 10:51:30",
  "timestamp": 1600591890
}
```

Sample update json file:

```json
{
  "last-event": "upgrade",
  "upgrade": {
    "from": "1.0.10",
    "time": "20200920 10:12:56",
    "timestamp": 1600589576,
    "to": "1.1.1"
  }
}
```

## Relative Paths ##

All relative paths in Guardian are related to Workspace's absolute path.

For example, suppose that workspace is "`/Users/myuser/app/workspace`".

If I write `./mydir/file.js` into any configuration file or script in Guardian, 
it's related to workspace and its value is "`/Users/myuser/app/workspace/mydir/file.js`".

## Workspace and Variables for Configuration files ##

Configuration file uses variables to better and simpler point at `workspace` 
or working directories.

`$dir_start`: Guardian starting directory

`$dir_app`: Guardian binary directory

`$dir_work`: Guardian Workspace

`$dir_modules`: Programs Directory

`$dir_programs`: Programs Directory

`$dir_program`: Program File Directory

Below is a sample `settings.json` file for GUARDIAN runtime:

```json
{
  "silent":false,
  "log-level": "debug",
  "actions": [
    {
      "uid": "nio_server",
      "start_at": "",
      "timeline": "second:1",
      "program": "./programs/nio_server.js",
      "updater": {
        "version_file": "http://localhost/scripts/nio_server/version.txt",
        "package_files": [
          {
            "file": "http://localhost/scripts/nio_server/nio_server.js",
            "target": "$dir_program"
          }
        ],
        "scheduled_updates": [
          {
            "uid": "every_5_seconds",
            "start_at": "",
            "timeline": "second:5"
          }
        ]
      }
    }
  ]
}
```

This configuration file uses `$dir_program` variable to define a target directory for live update.
```
    "package_files": [
      {
        "file": "http://localhost/scripts/nio_server/nio_server.js",
        "target": "$dir_program"
      }
    ]
```

When Guardian find a variable in a configuration file replace it with corresponding value.