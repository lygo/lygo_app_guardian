# Guardian Runtime #

Welcome to Guardian Runtime documentation.

Guardian Runtime is a cross-platform application able to run javascript files (agents).

Guardian is a programmable job orchestration system which allows for the **scheduling**, 
management and unattended background execution of user created tasks (agents) on Linux, 
Windows and Mac.

## Basic Concepts ##

### Scheduling ###

"Scheduling" is a basic concept in Guardian and is very important to understand how it works and how agents 
can be executed.

Guardian starts reading a [configuration](./system/configuration.md) file in its workspace (directory declared as a starting point).

### Live Update ###

"Live Update" is another very important feature in Guardian.
Every Agent can optionally be updated by an internal version control task.

Discover more about Live Update in [configuration](./system/configuration.md) documents.

### Workspace ###

Everything in Guardian live in the [workspace](./system/workspace.md).

Workspace is main working directory for Guardian runtime and hosts agent files (programs), modules, log files and more.

## Chapters ##

* [Configuration](./system/configuration.md)
* [Workspace](./system/workspace.md)
* [Programming](./programming/index.md)

## Guardian Playground ##

To better understand Guardian you can [play a while with our playground](https://bitbucket.org/lygo/lygo_app_guardian/src/master/_playground/).

Please, refer to [readme](https://bitbucket.org/lygo/lygo_app_guardian/src/master/_playground/readme.md) 
for better understand how to play with Guardian. 

