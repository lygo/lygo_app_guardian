## rnd-utils ##

```javascript
var rnd = require("rnd-utils"); // use("rnd-utils")

function main() {
    var response = {};
    try {
        response["guid"] = rnd.giud();
        response["time-guid"] = rnd.tgiud();
        response["6-digits"] = rnd.digits(6);
        response["between-3-100"] = rnd.between(3,100);
    } catch (err) {
        console.error(err);
        response.error = err;
    }
    return response;
}
```