## dbal ##

"Database Abstraction Layer" is a agnostic database interface.

**SAMPLE USAGE**
```javascript
var dbal = use("dbal");

// creates a connecton from scratch to database and do not use cache
// var db = dbal.create("mysql", "admin:admin@tcp(localhost:3306)/test");

// reuse a cached db connection or create from scratch if does not exists in cache
var db = dbal.get("mysql", "admin:admin@tcp(localhost:3306)/test");

// create a query in database language (here is SQL, but works on every supported dialect)
var command = db.query("SELECT * FROM table1");

// get data 
var result = command.exec();

```

DBAL can use ODBC, too.
```
var db = dbal.get("odbc", "server=192.168.1.1,1433;database=MYDB;uid=user1;pwd=1234124;TDS_Version=8.0;");

```