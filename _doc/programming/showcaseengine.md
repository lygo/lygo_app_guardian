## showcase-search ##

```javascript
var showcase = use("showcase-engine");

// creates engine instance
var engine = showcase.newEngine({
    "driver": "arango",
    "dsn": "root:password@tcp(localhost:8529)/mydatabase)"
});

// add some content to engine
var categories = ["event", "person", "post", "adv", "document"]
var entity = {
    "type":"event",
    "date": new Date().getTime()
};
var unix_timestamp = entity.date/1000;
engine.put(entity, categories[0], unix_timestamp);

var entity2 = {
    "type":"person",
    "firstName": "Mario",
    "lastName":"Rossi"
};
engine.put(entity2, categories[1]); // autocreated timespamp

// retrieve data
var session_id = "1234567890"; // this may be a user_id
// get 1 item
var items = engine.get(session_id);

// get 5 items
var more_items = engine.get(session_id, 5);

```