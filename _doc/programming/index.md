# Programming #

## Sample ##
```javascript
var path = require("path");

(function(){
	
    var response = {
        delimiter: path.delimiter,
        sep: path.sep,
        dirname: path.dirname("/dir/dir2/dir3/file"),
        basename: path.basename("/dir/dir2/dir3/file.html", ".html"),
        extname: path.extname("/dir/dir2/dir3/file.html"),
        format: path.format({
            "dir":"/home/dir1",
            "base":"file.html"
        }),
        isAbsolute: path.isAbsolute("/dir/dir2/dir3/file.html"),
        isAbsolute2: path.isAbsolute("file.html"),
        join: path.join("/dir/dir2", "dir3", "file.html", ".."),
        normalize: path.normalize('/foo/bar//baz/asdf/quux/..'),
        parse: path.parse('/foo/bar/baz/asdf/quux/file.txt'),
        relative: path.relative('/data/orandea/test/aaa', '/data/orandea/impl/bbb'),
        resolve: path.resolve('/foo/bar', './baz'),
        resolve2: path.resolve('/foo/bar', '/tmp/file/'),
        resolve3: path.resolve('wwwroot', 'static_files/png/', '../gif/image.gif'),

    };

    var s = JSON.stringify(response);
    console.debug(s);
    return s;

})();
```

## Modules ##

* [fs](fs.md) : File System Module
* [dbal](_docs/dbal.md): Database Abstraction Layer. Support MySQL, ArangoDB
* [http](http.md) : Http Client and Http Server (Express like) implementation
* [line-reader](linereader.md): Utility to read text files line-by-line
* [message-queue](messagequeue.md): RabbitMQ client implementation
* [nio](nio.md): Client/Server Network I/O 
* [nodemailer](nodemailer.md): Send Emails
* [path](path.md): Path utilities
* [showcase-engine](showcaseengine.md): Showcase/Timeline engine implementation
* [sql](sql.md): SQL implementation for MySQL and all supported drivers
* [sys](sys.md): System utilities and information
* [crypto-utils](utils_crypto.md): Utility for cryptography
* [file-utils](utils_file.md): Utility for file R/W
* [date-utils](utils_date.md): Utility for date parsing, formatting and manipulation
