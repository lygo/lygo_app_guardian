## auth0 ##

### Methods ###

**userSignIn**

`var loginData = auth.userSignIn("user1", "password1")`

**userConfirm**

`auth.userConfir(confirmToken)` // throw error if something wrong

**userSignUp**

`var loginData = auth.userSignUp("user1", "password1", payload)`

**userUpdate**

`var newUserId = auth.userUpdate(userId, newUserName, newPassword, payload)`

**tokenRefresh**

`var loginData = auth.tokenRefresh(refreshToken)`

**tokenValidate**

`var isValid = auth.tokenValidate(token)`

### Login Example ###

```
var auth0 = require('auth0');

// auth configuration
var config = {
    "secrets": {
        "auth": "this-is-token-to-authenticate",
        "access": "hsdfuhksdhf5435khjsd",
        "refresh": "hsdfuhqswe34qwksdhfkhjsd"
    },
    "cache-storage": {
        "driver": "arango",
        "dsn": "root:xxxxxx@tcp(localhost:8529)/test)"
    },
    "auth-storage": {
        "driver": "arango",
        "dsn": "root:xxxxxx@tcp(localhost:8529)/test)"
    }
};

try {
    var auth = auth.newEngine(config);
    auth.open();
    try {
        var loginData = auth.userSignIn("user1", "password1")
        if (!!loginData.error) {
            console.error(loginData.error);
        } else {
            // user logged
            console.log("USER ID: " + loginData.item_id);
            console.log("USER PAYLOAD: " + JSON.stringify(loginData.item_payload));
            console.log("TOKEN: " + loginData.access_token);
        }
    } finally {
        auth.close();
    }
} catch(err) {
    console.error(err);
}
```
