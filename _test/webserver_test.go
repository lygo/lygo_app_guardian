package _test

import (
	"bitbucket.org/lygo/lygo_ext_http/httpclient"
	"fmt"
	"sync"
	"sync/atomic"
	"testing"
	"time"
)

func TestWebserver(t *testing.T) {
	url := "http://localhost/api/v4/test"

	var runOk uint64
	var runErr uint64

	var w sync.WaitGroup
	for i := 0; i < 300; i++ {
		w.Add(1)
		go func() {
			defer w.Done()
			client := httpclient.NewHttpClient()
			_, err := client.Get(url)
			if nil != err {
				atomic.AddUint64(&runErr, 1)
				//fmt.Println(err.Error())
			} else {
				atomic.AddUint64(&runOk, 1)
				//fmt.Println(string(resp.Body))
			}
		}()
	}
	w.Wait()

	time.Sleep(time.Second)
	client := httpclient.NewHttpClient()
	resp, err := client.Get(url)
	if nil != err {
		fmt.Println(err.Error())
	} else {
		fmt.Println("LAST REQUEST TO TEST SERVER IS ALIVE", string(resp.Body))
	}

	fmt.Println("ERRORS", runErr)
	fmt.Println("OK", runOk)
}

func TestWebserverQuery(t *testing.T) {
	url := "http://localhost/api/v4/query"

	var runOk uint64
	var runErr uint64

	var w sync.WaitGroup
	for i := 0; i < 500; i++ {
		w.Add(1)
		go func() {
			defer w.Done()
			client := httpclient.NewHttpClient()
			body := map[string]interface{}{
				"key": "12345",
				"name": "foo",
			}
			_, err := client.Post(url, body)
			if nil != err {
				atomic.AddUint64(&runErr, 1)
				//fmt.Println(err.Error())
			} else {
				atomic.AddUint64(&runOk, 1)
				//fmt.Println(string(resp.Body))
			}
		}()
	}
	w.Wait()

	time.Sleep(time.Second)
	client := httpclient.NewHttpClient()
	body := map[string]interface{}{
		"key": "12345",
	}
	resp, err := client.Post(url, body)
	if nil != err {
		fmt.Println(err.Error())
	} else {
		fmt.Println("LAST REQUEST TO TEST SERVER IS ALIVE", string(resp.Body))
	}

	fmt.Println("ERRORS", runErr)
	fmt.Println("OK", runOk)
}
