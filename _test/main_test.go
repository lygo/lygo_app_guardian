package _test

import (
	"bitbucket.org/lygo/lygo_app_guardian/app"
	"bitbucket.org/lygo/lygo_app_guardian/app/commons"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_commons/lygo_strings"
	"testing"
	"time"
)

var (
	testCounters = []map[string]string{
		{"timeline": "second:1", "program": "counter_1.js"},
		{"timeline": "second:3", "program": "counter_2.js"},
	}
	testNio = []map[string]string{
		{"timeline": "second:1", "program": "nio_server.js"},
		{"uid": "nio_client_1", "timeline": "second:1", "program": "nio_client.js"},
		{"uid": "nio_client_2", "timeline": "second:1", "program": "nio_client.js"},
		{"uid": "nio_client_3", "timeline": "millisecond:333", "program": "nio_client.js"},
	}
	testFileSize = []map[string]string{
		{"timeline": "second:3", "program": "check_file_size.js"},
	}
)

func TestLaunch(t *testing.T) {

	mode := commons.ModeDebug

	lygo_paths.SetWorkspacePath(lygo_paths.Absolute("../_guardian"))

	tests := [][]map[string]string{testNio, testNio}
	wait := 5 * time.Second

	// execute all tests with multiple guardian instances
	for _, testSettings := range tests {
		settings := commons.NewGuardianSettingsFromData("./programs/", testSettings)
		guardian, err := app.NewGuardian(mode, settings)
		if nil == err {
			// get logger from guardian
			logger := guardian.GetLogger()
			message := lygo_strings.Format("APPLICATION %s v.%s mode '%s' START: %s", commons.Name, commons.Version, mode, lygo_paths.GetWorkspacePath())
			logger.Info(message)

			err = guardian.Start()
			if nil != err {
				panic(err)
			}

			guardian.WaitTimeout(wait) // lock waiting guardian exit or close gracefully
		}
	}
}

// ---------------------------------------------------------------------------------------------------------------------
//		p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------
