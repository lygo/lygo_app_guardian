##!/bin/sh

BASE="1.1"
# Get latest build
BUILD=$(<build_version.txt)
chrlen=${#BUILD}
if [ $chrlen = 0 ]
then
  BUILD=0
fi

echo "START BUILDING LINUX ARM64 VERSION $BASE.$BUILD..."

## linux
env GOOS=linux GOARCH=arm64 go build -o ./_build/linux_arm64/guardian main.go
